
#include "symbol.h"
#include <stdlib.h>
#include <string.h>

#include "ast.h"

// helper structure for scoping
struct scopeVarCount {
    int count;
    struct scopeVarCount * next;
};

// helper structure to create variable stacks
struct decaredVariable {
    
    // information of the variable
    node_kind type;
    char * name;
    
     // const, uniform, attribute, result
    iden_flag flag;
    int flaghelper; 
    
    // assignment
    int varIsSet;
    int varhelper;
    
    // value
    int size;
    float value[4];
    
    // next variable in stack
    struct decaredVariable * next;
};

// variable for saving variable name and count
struct decaredVariable * stackhead = NULL;
struct scopeVarCount * scopehead = NULL;

int pushProgramVariables(){
    if(pushScope() == 0) return 0;
    
    char name [30];
    memset(name, 0, 30);
    strcpy(name, "gl_FragColor");
    if(pushVariable(NKIND_TYPE_VEC4,name) == 0)return 0;
    setFlag(name, FLAG_RESULT);
        
    memset(name, 0, 30);
    strcpy(name, "gl_FragDepth");
    if(pushVariable(NKIND_TYPE_VEC4,name) == 0)return 0;
    setFlag(name, FLAG_RESULT);
    
    memset(name, 0, 30);
    strcpy(name, "gl_FragCoord");
    if(pushVariable(NKIND_TYPE_VEC4,name) == 0)return 0;
    setFlag(name, FLAG_RESULT);
    
    memset(name, 0, 30);
    strcpy(name, "gl_TexCoord");
    if(pushVariable(NKIND_TYPE_VEC4,name) == 0)return 0;
    setFlag(name, FLAG_ATTRIBUTE);
    setValue( name, 0, 4, 0.0, 0.0, 0.0, 0.0);
    setVarToAssigned(name);
    
    memset(name, 0, 30);
    strcpy(name, "gl_Color");
    if(pushVariable(NKIND_TYPE_VEC4,name) == 0)return 0;
    setFlag(name, FLAG_ATTRIBUTE);
    setValue( name, 0, 4, 0.0, 0.0, 0.0, 0.0);
    setVarToAssigned(name);
    
    memset(name, 0, 30);
    strcpy(name, "gl_Secondary");
    if(pushVariable(NKIND_TYPE_VEC4,name) == 0)return 0;
    setFlag(name, FLAG_ATTRIBUTE);
    setValue( name, 0, 4, 0.0, 0.0, 0.0, 0.0);
    setVarToAssigned(name);
    
    memset(name, 0, 30);
    strcpy(name, "gl_FogFragCoord");
    if(pushVariable(NKIND_TYPE_VEC4,name) == 0)return 0;
    setFlag(name, FLAG_ATTRIBUTE);
    setValue( name, 0, 4, 0.0, 0.0, 0.0, 0.0);
    setVarToAssigned(name);
    
    memset(name, 0, 30);
    strcpy(name, "gl_Light_Half");
    if(pushVariable(NKIND_TYPE_VEC4,name) == 0)return 0;
    setFlag(name, FLAG_UNIFORM);
    setValue( name, 0, 4, 0.0, 3.0, 5.0, 0.0);
    setVarToAssigned(name);
    
    memset(name, 0, 30);
    strcpy(name, "gl_Light_Ambient");
    if(pushVariable(NKIND_TYPE_VEC4,name) == 0)return 0;
    setFlag(name, FLAG_UNIFORM);
    setValue( name, 0, 4, 0.0, 0.0, 0.0, 0.0);
    setVarToAssigned(name);
    
    memset(name, 0, 30);
    strcpy(name, "gl_Material_Shininess");
    if(pushVariable(NKIND_TYPE_VEC4,name) == 0)return 0;
    setFlag(name, FLAG_UNIFORM);
    setValue( name, 0, 4, 0.0, 0.0, 0.0, 0.0);
    setVarToAssigned(name);
    
    memset(name, 0, 30);
    strcpy(name, "env1");
    if(pushVariable(NKIND_TYPE_VEC4,name) == 0)return 0;
    setFlag(name, FLAG_UNIFORM);
    setValue( name, 0, 4, 0.0, 0.0, 0.0, 0.0);
    setVarToAssigned(name);
    
    memset(name, 0, 30);
    strcpy(name, "env2");
    if(pushVariable(NKIND_TYPE_VEC4,name) == 0)return 0;
    setFlag(name, FLAG_UNIFORM);
    setValue( name, 0, 4, 0.0, 0.0, 0.0, 0.0);
    setVarToAssigned(name);
    
    memset(name, 0, 30);
    strcpy(name, "env3");
    if(pushVariable(NKIND_TYPE_VEC4,name) == 0)return 0;
    setFlag(name, FLAG_UNIFORM);
    setValue( name, 0, 4, 0.0, 0.0, 0.0, 0.0);
    setVarToAssigned(name);
    
    return 1;
}

int pushScope(){
    struct scopeVarCount * scopepush = (struct scopeVarCount*) malloc(sizeof (struct scopeVarCount));
    if (scopepush == NULL) {return 0;}
    
    scopepush->count = 0;
    scopepush->next = scopehead;
    scopehead = scopepush;
    return 1;
}

int pushVariable(node_kind itype, char * iname) {

    // error checking
    if (scopehead == NULL) return 0;

    // new variable
    struct decaredVariable * push = (struct decaredVariable*) malloc(sizeof (struct decaredVariable));
    if(push == NULL) return 0;
    
    // variable information
    push->type = itype;
    
    push->name = (char*)malloc(sizeof(char)*(strlen(iname)+1));
    memcpy(push->name, iname, strlen(iname)+1);
    
    // flag
    push->flag = FLAG_REGULAR;
    push->flaghelper = 0;
       
     // assignment
    push->varIsSet = 0;
    push->varhelper = 1;
    
    // value
    push->size = 0;
    int i;
    for(i = 0; i< 4; i++)
        push->value[i] = 0;
    
    // next
    push->next = stackhead;

    // insert at head
    stackhead = push;

    // increment counting
    scopehead->count++;
    return 1;
}

int popVariable() {

    // could not pop if nothing
    if (stackhead == NULL) return 0;
    if (scopehead == NULL) return 0;

    // pop as scope head told
    while (scopehead->count > 0) {

        // decrement
        scopehead->count--;

        // poping
        struct decaredVariable * pop = stackhead;
        stackhead = stackhead->next;
        
        free(pop->name);
        free(pop);
    }

    // remove counting
    struct scopeVarCount * tmp = scopehead;
    scopehead = scopehead->next;
    free(tmp);

    // could not find the node
    return 1;
}

search_variable getVarKind(char * sname, node_kind * result) {
    // stack operation, find
    
    struct decaredVariable * stack = stackhead;
    int counting = 0;
    
    while (stack != NULL) {
        if (strcmp(sname, stack->name) == 0) {
            *result = stack->type;
            
            // see if it is in the same scope
            if(counting < scopehead->count)
                return SCOPE_FOUND_SAME;
            else
                return SCOPE_FOUND_DIFF;
        }
        stack = stack->next;
        counting ++;
    }
    
    *result = NKIND_FAIL;
    return SCOPE_NOTFOUND;
    
}

void getFlag(char *name, iden_flag * result){
    struct decaredVariable * stack = stackhead;
    while (stack != NULL) {
        if (strcmp(name, stack->name) == 0) {
            // this is for escaping the constant assignment
            if(stack->flaghelper > 0){
                *result = FLAG_REGULAR;
                stack->flaghelper --;
            }
            else
                *result = stack->flag;
            
            return;
        }
        stack = stack->next;
    }
    
    return;
}

void setFlag(char *name, iden_flag flag){
    struct decaredVariable * stack = stackhead;   
    while (stack != NULL) {
        if (strcmp(name, stack->name) == 0) {
            stack->flag = flag;
            
            // if it is a constant. Set a helper flag for escaping first assignment
            if(flag == FLAG_CONST)  stack->flaghelper = 2;
            return;
        }
        // to next
        stack = stack->next;
    }
    // could not find the variable. 
    // which should not happen
    return;
}

float* getValue(char * name, int *size){
    struct decaredVariable * stack = stackhead;
    while (stack != NULL) {
        if (strcmp(name, stack->name) == 0) {
            *size = stack->size;
            return stack->value;
        }
         stack = stack->next;
    }
    // couldn't find the value
    // which should not happen?
    return NULL;
}
void setValue(char * name, int isInt, int size, float v0, float v1, float v2, float v3){
    struct decaredVariable * stack = stackhead;
    while (stack != NULL) {
        if (strcmp(name, stack->name) == 0) {
            stack->size = size;
            if(size > 0)    stack->value[0] = (isInt)? (int)v0 : v0;
            if(size > 1)    stack->value[1] = (isInt)? (int)v1 : v1;
            if(size > 2)    stack->value[2] = (isInt)? (int)v2 : v2;
            if(size > 3)    stack->value[3] = (isInt)? (int)v3 : v3;
            return;
        }
         stack = stack->next;
    }
    // couldn't find the value
    // which should not happen?
    return;
}


int getVarAssigned(char * name){
    struct decaredVariable * stack = stackhead;
    while (stack != NULL) {
        if (strcmp(name, stack->name) == 0) {
            if(stack->varhelper > 0){
                stack->varhelper--;
                return 1;
            }
            else
                return stack->varIsSet;
        }
         stack = stack->next;
    }
    return 0;
}
void setVarToAssigned(char * name){
     struct decaredVariable * stack = stackhead;
    while (stack != NULL) {
        if (strcmp(name, stack->name) == 0) {
            stack->varIsSet = 1;
            stack->varhelper = 0;
            return;
        }
         stack = stack->next;
    }
    return;
}

#include <stdio.h>
int getVarScope(char *name){
    
    // count the index of variable on the stack
    int countvariable = 0;
    struct decaredVariable * stack = stackhead;
    while (stack != NULL){
        
        countvariable ++;
        if(strcmp(stack->name,name) == 0)
            break;
        stack = stack->next;
    }
        
    // find the scope
    struct scopeVarCount * scope = scopehead;
    while(scope != NULL){
        
        countvariable -= scope->count;
        if(scope->count >= countvariable)
            break;
        scope = scope->next;
    }
    
    
    // find out the level of this scope
    int scopelevel = 0;
    while(scope!= NULL){
        scopelevel ++;
        scope = scope-> next;
    }
    
    return scopelevel -1;
}