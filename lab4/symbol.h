#ifndef _SYMBOL_H 
#define _SYMBOL_H

#include "ast.h"

typedef enum {
    SCOPE_FOUND_SAME,
    SCOPE_FOUND_DIFF,
    SCOPE_NOTFOUND
} search_variable;

typedef enum{

    FLAG_REGULAR,
    FLAG_RESULT,
    FLAG_ATTRIBUTE,
    FLAG_UNIFORM,
    FLAG_CONST
            
}iden_flag;

/* For creating predefined variable that is in program scope */
int pushProgramVariables();

/* When entering a scope, need to call this function */
int pushScope();

/* Push a variable into stack*/
int pushVariable(node_kind itype, char * iname);

/* Pop all variable in this scope*/
int popVariable();

/* Given a name, find the variable kind*/
search_variable getVarKind(char * sname, node_kind * result);

/* Given a name, find variable flag*/
void getFlag(char *name, iden_flag * result);
void setFlag(char *name, iden_flag flag);

/* Given a name, find variable value*/
float* getValue(char * name, int *size);
void setValue(char * name, int isInt, int size, float v0, float v1, float v2, float v3);

/* Given a name, get if the variable is assigned flag*/
int getVarAssigned(char * name);
void setVarToAssigned(char * name);

int getVarScope(char *name);

#endif
