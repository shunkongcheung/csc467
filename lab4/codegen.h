/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   codegen.h
 * Author: cheun673
 *
 * Created on November 24, 2017, 5:18 PM
 */

#ifndef CODEGEN_H
#define CODEGEN_H

#include "ast.h"

void genCode(struct node *node);

#endif /* CODEGEN_H */

