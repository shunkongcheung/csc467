/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   arb.c
 * Author: cheun673
 *
 * Created on November 24, 2017, 5:15 PM
 */

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include "codegen.h"
#include "semantic.h"
#include "symbol.h"


#define tempMax 10

struct tempList {
    int index;
    struct tempList * next;
};

// temp variable
struct tempList * myVarList = NULL;
int declaredVarCount;
int assignedVar[tempMax] = {0};

// function that work with tempList
void pushList(int tempIndex);
int removeFromList();

void genRecursive(struct node * node, FILE * f, int *ifCount, int isElse);
void genDeclaration(struct node * node, FILE * f);

void genAssignmentRegular(struct node * node, FILE *f, int isInIf);
void genAssignmentIndex(struct node * node, FILE *f, int isInIf);
void genAssignmentVectors(struct node * node, FILE *f, int isInIf);

void genExpression(struct node * node, FILE *f);
void genFunctions(struct node * node, FILE *f);

// convert variable name for predefined variables
// return 1 if result is valid.
// return 0 if no need to change
int resolveVariableName(char * result, char * name, int index);

// operation is the string for output
// generate result by identifying the type of kids:
// it can be a function call / a constant / a variable
// by that generate strings 
void genTypeArgument(struct node * node, char* operation);


void genAndOrNot(struct node *node, FILE * f);
void genCMPCondition(struct node* node, FILE *f);

void genCode(struct node * node) {
    //generate code wrapper

    // initialized
    FILE *file = fopen("frag.txt", "w+");

    declaredVarCount = 0;

    // start recursion
    int ifCount = 0;
    genRecursive(node, file, &ifCount, 0);

    // finish
    fclose(file);
    return;
}

void genRecursive(struct node * node, FILE * f, int *ifCount, int isElse) {
    // should be success if child is null
    // some kind of node may have null child
    
    
    if (node == NULL) {
        return;
    }

    if (node->mykind == NKIND_SCOPE) {
        pushScope();
    }
    if (node->mykind == NKIND_IF) {
        *ifCount = *ifCount + 1;

        // find the first unassigned temp variable
        int ifTempIndex = 0;
        while (ifTempIndex < declaredVarCount && assignedVar[ifTempIndex] == 1) {
            ifTempIndex++;
        }

        // if need to declare
        if (ifTempIndex == declaredVarCount) {
            fprintf(f, "TEMP tempVar%d;\n", ifTempIndex);
            declaredVarCount++;
        }

        double value[4] = {0};
        int isInt = 0;
        int length = getExpressionValue(node->kids[0], value, &isInt);

        if (length >= 1) {
            // get operation
            char operation[200];
            memset(operation, 0, 200);

            sprintf(operation + strlen(operation), "MOV tempVar%d, %d;\n", ifTempIndex, (int)value[0]);
            
            // put to file
            fprintf(f, operation);
            
            pushList(ifTempIndex);
        }
    }

    genDeclaration(node, f);

    /* Done checking this node! traverse to children */
    int childindex = 0;
    for (childindex = 0; childindex < node->nkids; childindex++) {
        genRecursive(node->kids[childindex], f, ifCount, (node->mykind == NKIND_ELSE) ? 1 : 0);

        if (node->mykind == NKIND_ELSE && childindex == 0) {

            // find the first unassigned temp variable
            int tempIndex = 0;
            while (tempIndex < declaredVarCount && assignedVar[tempIndex] == 1) {
                tempIndex++;
            }

            // if need to declare
            if (tempIndex == declaredVarCount) {
                fprintf(f, "TEMP tempVar%d;\n", tempIndex);
                declaredVarCount++;
            }

            char firstname[200];
            memset(firstname, 0, 200);
            genTypeArgument(node->kids[0], firstname);
            // same as not
            fprintf(f, "CMP tempVar%d, %s, 0, 1\n", tempIndex, firstname);

            pushList(tempIndex);
        }

        if (node->mykind == NKIND_IF && childindex == 0 && *ifCount > 1) {
            // nested if

            // the latest condition AND all previous condition
            int latest = myVarList->index;
            struct tempList * loop = myVarList->next;
            for (int i = 0; i < (*ifCount - 1); i++) {
                fprintf(f, "CMP tempVar%d, tempVar%d, tempVar%d, 0\n", latest, latest, loop->index);
                loop = loop->next;
            }
        }

    }

    genExpression(node, f);
    genFunctions(node, f);

    genAssignmentIndex(node, f, (*ifCount > 0) ? 1 : 0);
    genAssignmentRegular(node, f, (*ifCount > 0) ? 1 : 0);
    genAssignmentVectors(node, f, (*ifCount > 0) ? 1 : 0);

    genAndOrNot(node, f);
    genCMPCondition(node, f);

    if (node->mykind == NKIND_SCOPE) {
        popVariable();
    }
    if (node->mykind == NKIND_IF && !isElse) {
        removeFromList();
        *ifCount = *ifCount - 1;
    }
    if(isElse)
    if (node->mykind == NKIND_ELSE) {
        removeFromList();
        *ifCount = *ifCount - 1;
    }

}

void genAndOrNot(struct node *node, FILE * f) {

    if (!(node->mykind == NKIND_EXP_AND ||
            node->mykind == NKIND_EXP_OR ||
            node->mykind == NKIND_EXP_NOT))
        return;

    // find the first unassigned temp variable
    int tempIndex = 0;
    while (tempIndex < declaredVarCount && assignedVar[tempIndex] == 1) {
        tempIndex++;
    }

    // if need to declare
    if (tempIndex == declaredVarCount) {
        fprintf(f, "TEMP tempVar%d;\n", tempIndex);
        declaredVarCount++;
    }

    // And and Or needs two condition (may be a variable or an expression)
    // Not only needs one
    char firstname[200];
    memset(firstname, 0, 200);

    char secondname[200];
    memset(secondname, 0, 200);


    // AND and OR need two condition
    // Not only need one
    genTypeArgument(node->kids[0], firstname);

    if (node->mykind == NKIND_EXP_NOT) {
        // flip value
        fprintf(f, "CMP tempVar%d, %s, 0, 1\n", tempIndex, firstname);

    } else if (node->mykind == NKIND_EXP_AND) {
        // if this is false, always false, if true, depend on second condition
        genTypeArgument(node->kids[1], secondname);
        fprintf(f, "CMP tempVar%d, %s, %s, 0\n", tempIndex, firstname, secondname);

    } else {
        // if this is true, always true, if it is false, depends on second condition
        genTypeArgument(node->kids[1], secondname);
        fprintf(f, "CMP tempVar%d, %s, 1, %s\n", tempIndex, firstname, secondname);

    }

    pushList(tempIndex);
    return;
}

void genCMPCondition(struct node *node, FILE * f) {


    if (!(node->mykind == NKIND_EXP_COMP))
        return;

    // find the first unassigned temp variable
    int tempIndex = 0;
    while (tempIndex < declaredVarCount && assignedVar[tempIndex] == 1) {
        tempIndex++;
    }

    // if need to declare
    if (tempIndex == declaredVarCount) {
        fprintf(f, "TEMP tempVar%d;\n", tempIndex);
        declaredVarCount++;
    }


    char leftname [200];
    memset(leftname, 0, 200);
    genTypeArgument(node->kids[0], leftname);


    char rightname [200];
    memset(rightname, 0, 200);
    genTypeArgument(node->kids[2], rightname);

    fprintf(f, "SUB tempVar%d %s %s\n", tempIndex, leftname, rightname);


    node_kind compkind = node->kids[1]->mykind;
    if (compkind == NKIND_COMP_EQUAL) {
        // if equal, tempVar will equal to zero
        // therefore tempVar = (tempVar != 0)? 0: 1;
        fprintf(f, "CMP tempVar%d, tempVar%d, 0, 1\n", tempIndex, tempIndex);

    } else if (compkind == NKIND_COMP_NOT_EQUAL) {
        // tempVar = (tempVar != 0)? 1 : 0
        fprintf(f, "CMP tempVar%d, tempVar%d, 1, 0\n", tempIndex, tempIndex);

    } else if (compkind == NKIND_COMP_GREATER) {
        // tempVar = (tempVar != 0)? tempVar : -1
        fprintf(f, "CMP tempVar%d, tempVar%d, tempVar%d, -1\n", tempIndex, tempIndex, tempIndex);

        // tempVar = (tempVar >= 0)? 1: tempVar,
        // since there is no == 0, it equals as tempVar > 0
        fprintf(f, "SGE tempVar%d, tempVar%d, 1\n", tempIndex, tempIndex);

        // tempVar = (tempVar < 0)? 0
        fprintf(f, "SLT tempVar%d, tempVar%d, 0\n", tempIndex, tempIndex);

    } else if (compkind == NKIND_COMP_LESS) {

        // tempVar = (tempVar >= 0)? 0 
        fprintf(f, "SGE tempVar%d, tempVar%d, 0\n", tempIndex, tempIndex);

        // tempVar = (tempVar < 0)? 1
        fprintf(f, "SLT tempVar%d, tempVar%d, 1\n", tempIndex, tempIndex);

    } else if (compkind == NKIND_COMP_GREATER_EQUAL) {

        fprintf(f, "SGE tempVar%d, tempVar%d, 1\n", tempIndex, tempIndex);
        fprintf(f, "SLT tempVar%d, tempVar%d, 0\n", tempIndex, tempIndex);

    } else if (compkind == NKIND_COMP_LESS_EQUAL) {

        fprintf(f, "CMP tempVar%d, tempVar%d, tempVar%d, -1\n", tempIndex, tempIndex, tempIndex);
        fprintf(f, "SGE tempVar%d, tempVar%d, 0\n", tempIndex, tempIndex);
        fprintf(f, "SLT tempVar%d, tempVar%d, 1\n", tempIndex, tempIndex);
    } else {
        printf("WTF?\n");
    }

    // push temp variable
    pushList(tempIndex);
}

void genDeclaration(struct node * node, FILE * f) {
    //Generate register for variable

    if (!(node->mykind == NKIND_DECLARE_CONST ||
            node->mykind == NKIND_DECLARATION))
        return;

    // get name
    struct node * idennode = NULL;
    if (node->kids[1]->mykind == NKIND_IDENT) {
        idennode = node->kids[1];
    } else {
        idennode = node->kids[1]->kids[0];
    }

    // push this variable to scope
    pushVariable(idennode->mykind, idennode->iden);

    // print to file
    fprintf(f, "TEMP %s%d;\n", idennode->iden, getVarScope(idennode->iden));

    return;
}

void genAssignmentIndex(struct node * node, FILE *f, int isInIf) {
    if (!(node->mykind == NKIND_ASSIGNMENT_INDEX))
        return;

    char name[200];
    memset(name, 0, 200);
    resolveVariableName(name, node->kids[0]->iden, node->kids[1]->numint);

    // get operation
    char operation[200];
    memset(operation, 0, 200);

    
    char temp[200];
    memset(temp, 0, 200);
    genTypeArgument(node->kids[2], temp);
     
    if (isInIf) {
        sprintf(operation + strlen(operation), "CMP %s, tempVar%d, ", name, myVarList->index);
    } else {
        sprintf(operation + strlen(operation), "MOV %s, ", name);
    }
    
    sprintf(operation + strlen(operation),"%s", temp);

    if (isInIf) {
        sprintf(operation + strlen(operation), ", %s", name);
    }

    sprintf(operation + strlen(operation), ";\n");

    // put to file
    fprintf(f, operation);
    return;


}

void genAssignmentRegular(struct node * node, FILE *f, int isInIf) {

    if (!(node->mykind == NKIND_ASSIGNMENT_REGULAR))
        return;

    // name 
    char * name;
    if (node->mykind == NKIND_ASSIGNMENT_REGULAR)
        name = node->kids[0]->iden;


    // get operation
    char operation[200];
    memset(operation, 0, 200);

    if (isInIf) {
        sprintf(operation + strlen(operation), "CMP %s%d, tempVar%d, ", name, getVarScope(name), myVarList->index);
    } else {
        sprintf(operation + strlen(operation), "MOV %s%d, ", name, getVarScope(name));
    }

    genTypeArgument(node->kids[1], operation);

    if (isInIf) {
        sprintf(operation + strlen(operation), ", %s%d", name, getVarScope(name));
    }

    sprintf(operation + strlen(operation), ";\n");

    // put to file
    fprintf(f, operation);
    return;
}

void genAssignmentVectors(struct node * node, FILE *f, int isInIf) {

    if (!(node->mykind == NKIND_ASSIGNMENT_VEC2 ||
            node->mykind == NKIND_ASSIGNMENT_VEC3 ||
            node->mykind == NKIND_ASSIGNMENT_VEC4))
        return;

    // name 
    char * name = node->kids[0]->iden;

    // loop throught all values
    int loopvec = 0;
    int loopend = 4;
    if (node->mykind == NKIND_ASSIGNMENT_VEC2)loopend = 2;
    if (node->mykind == NKIND_ASSIGNMENT_VEC3)loopend = 3;

    // assignment is not a constant..
    char operation[200];
    memset(operation, 0, 200);


    if (isInIf) {
        sprintf(operation + strlen(operation), "CMP %s%d, tempVar%d, {", name, getVarScope(name), myVarList->index);
    } else {
        sprintf(operation + strlen(operation), "MOV %s%d, {", name, getVarScope(name));
    }

    //    sprintf(operation + strlen(operation), "MOV %s%d, {", name, getVarScope(name));
    for (loopvec = 0; loopvec < loopend; loopvec++) {

        genTypeArgument(node->kids[loopvec + 1], operation);
        if (loopvec != loopend - 1)
            sprintf(operation + strlen(operation), ", ");
    }


    if (isInIf) {
        sprintf(operation + strlen(operation), "}, %s%d\n", name, getVarScope(name));

    } else {
        sprintf(operation + strlen(operation), "};\n");
    }
    fprintf(f, operation);

}

void genTypeArgument(struct node * node, char *operation) {
    // if it is a constant

    double value[4] = {0};
    int isInt = 0;
    int length = getExpressionValue(node, value, &isInt);

    if (length >= 1) {

        if (length != 1)
            sprintf(operation + strlen(operation), "{");

        for (int i = 0; i < length; i++) {
            if (isInt)
                sprintf(operation + strlen(operation), "%d", (int) value[i]);
            else
                sprintf(operation + strlen(operation), "%lf", value[i]);

            if (length != 1 && i != (length - 1))
                sprintf(operation + strlen(operation), ", ");
        }

        if (length != 1)
            sprintf(operation + strlen(operation), "}");

    } else if (node->mykind == NKIND_IDENT) {
        // if the assignment is a identifier

        char resolvedname [50];
        memset(resolvedname, 0, 50);

        resolveVariableName(resolvedname, node->iden, -1);
        sprintf(operation + strlen(operation), "%s", resolvedname);

    } else if (node->mykind == NKIND_IDENT_INDEX) {
        // if assignment is a indexed identifier

        char resolvedname [50];
        memset(resolvedname, 0, 50);

        resolveVariableName(resolvedname, node->kids[0]->iden, node->kids[1]->numint);
        sprintf(operation + strlen(operation), "%s", resolvedname);

    } else {
        // if assignment is an expression or a function
        int index = removeFromList();
        sprintf(operation + strlen(operation), "tempVar%d", index);
    }
}

void genFunctions(struct node * node, FILE *f) {

    char operation [100];
    memset(operation, 0, 100);

    if (node->mykind == NKIND_FUN_RSQ) {
        strcpy(operation, "RSQ ");
    } else if (node->mykind == NKIND_FUN_DP3) {
        strcpy(operation, "DP3 ");
    } else if (node->mykind == NKIND_FUN_LIT) {
        strcpy(operation, "LIT ");
    }
    //if node is not expression 
    if (operation[0] == 0)
        return;

    // find the first unassigned temp variable
    int tempIndex = 0;
    while (tempIndex < declaredVarCount && assignedVar[tempIndex] == 1) {
        tempIndex++;
    }

    // if need to declare
    if (tempIndex == declaredVarCount) {
        fprintf(f, "TEMP tempVar%d;\n", tempIndex);
        declaredVarCount++;
    }

    // put the assignment 
    sprintf(operation + strlen(operation), " tempVar%d, ", tempIndex);

    int argloop = 0;
    for (; argloop < node->nkids; argloop++) {

        genTypeArgument(node->kids[argloop], operation);

        if (argloop != node->nkids - 1)
            sprintf(operation + strlen(operation), ", ");
    }

    sprintf(operation + strlen(operation), ";\n");
    fprintf(f, operation);

    // push temp variable
    pushList(tempIndex);

}

void genExpression(struct node * node, FILE *f) {

    char operation [100];
    memset(operation, 0, 100);

    if (node->mykind == NKIND_EXP_PLUS) {
        strcpy(operation, "ADD");
    } else if (node->mykind == NKIND_EXP_MINUS) {
        strcpy(operation, "SUB");
    } else if (node->mykind == NKIND_EXP_MULTI) {
        strcpy(operation, "MUL");
    } else if (node->mykind == NKIND_EXP_DIVIDE) {
      
        strcpy(operation, "RCP");
    } else if (node->mykind == NKIND_EXP_NEG) {
        // negation = 0 - value
        strcpy(operation, "SUB");

    } else if (node->mykind == NKIND_EXP_EXPO) {
        strcpy(operation, "POW");
    }
    //if node is not expression 
    if (operation[0] == 0)
        return;

    // if this expression is a constant, no need to assign variable
    double value[4];
    int isInt = 0;
    int length = getExpressionValue(node, value, &isInt);
    if (length > 0)
        return;

    // find the first unassigned temp variable
    int tempIndex = 0;
    while (tempIndex < declaredVarCount && assignedVar[tempIndex] == 1) {
        tempIndex++;
    }

    // if need to declare
    if (tempIndex == declaredVarCount) {
        fprintf(f, "TEMP tempVar%d;\n", tempIndex);
        declaredVarCount++;
    }

    // put the assignment 
    sprintf(operation + strlen(operation), " tempVar%d, ", tempIndex);

    if(node->mykind == NKIND_EXP_NEG){
        sprintf(operation + strlen(operation), "0 , ");
    }
    if(node->mykind == NKIND_EXP_DIVIDE){
         
        genTypeArgument(node->kids[1], operation);
        sprintf(operation + strlen(operation), ";\n");
        fprintf(f, operation);

        memset(operation, 0, 100);
        sprintf(operation + strlen(operation), "MUL tempVar%d, tempVar%d, ", tempIndex, tempIndex);
        genTypeArgument(node->kids[0], operation);
        sprintf(operation + strlen(operation), ";\n");
        fprintf(f, operation);

        
         pushList(tempIndex);
    }
    
    
    int c = 0;
    for (; c < node->nkids; c++) {

        genTypeArgument(node->kids[c], operation);
        // any assignemnt is done
        if (c != node->nkids - 1)
            sprintf(operation + strlen(operation), ", ");
    }

    // push temp variable
    pushList(tempIndex);

    sprintf(operation + strlen(operation), ";\n");
    fprintf(f, operation);
    return;
}

void pushList(int tempIndex) {
    //use this TEMP register

    assignedVar[tempIndex] = 1;
    struct tempList *push = (struct tempList *) malloc(sizeof (struct tempList));
    push->index = tempIndex;
    push->next = myVarList;
    myVarList = push;

}

int removeFromList() {
    struct tempList *loop = myVarList;
    int index = -1;

    if(myVarList == NULL)
        return index;
    
    index = myVarList->index;
    loop = myVarList;
    myVarList = myVarList->next;
        
    // finishing
    assignedVar[index] = 0;
    free(loop);
    return index;
}

int resolveVariableName(char * result, char * name, int index) {

    if (strcmp(name, "gl_FragColor") == 0) {
        sprintf(result + strlen(result), "result.color");

    } else if (strcmp(name, "gl_FragDepth") == 0) {
        sprintf(result + strlen(result), "result.depth");

    } else if (strcmp(name, "gl_FragCoord") == 0) {
        sprintf(result + strlen(result), "fragment.position");

    } else if (strcmp(name, "gl_TexCoord") == 0) {
        sprintf(result + strlen(result), "fragment.texcoord");

    } else if (strcmp(name, "gl_Color") == 0) {
        sprintf(result + strlen(result), "fragment.color");

    } else if (strcmp(name, "gl_Secondary") == 0) {
        sprintf(result + strlen(result), "fragment.color.secondary");

    } else if (strcmp(name, "gl_FogFragCoord") == 0) {
        sprintf(result + strlen(result), "fragment.fogcoord");

    } else if (strcmp(name, "gl_Light_Half") == 0) {
        sprintf(result + strlen(result), "state.light[0].half");

    } else if (strcmp(name, "gl_Light_Ambient") == 0) {
        sprintf(result + strlen(result), "state.lightmodel.ambient");

    } else if (strcmp(name, "gl_Material_Shininess") == 0) {
        sprintf(result + strlen(result), "state.material.shininess");

    } else if (strcmp(name, "env1") == 0) {
        sprintf(result + strlen(result), "program.env[1]");

    } else if (strcmp(name, "env2") == 0) {
        sprintf(result + strlen(result), "program.env[2]");

    } else if (strcmp(name, "env3") == 0) {
        sprintf(result + strlen(result), "program.env[3]");
    } else {
        sprintf(result + strlen(result), "%s%d", name, getVarScope(name));
    }

    switch (index) {
        case 0:sprintf(result + strlen(result), ".x");
            break;
        case 1:sprintf(result + strlen(result), ".y");
            break;
        case 2:sprintf(result + strlen(result), ".z");
            break;
        case 3:sprintf(result + strlen(result), ".a");
            break;
        default:break;
    }

    return 1;
}