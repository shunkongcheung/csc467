
#remake
make clean
make 
clear

#checking correct outputting
compiler467  -Da ./test_cases/test_correct_semantics.c -E output_correct_errorfile -U output_correct_dumpfile 
#diff output_correct_dumpfile ./test_cases/expected_correct_dumpfile
#diff output_correct_errorfile ./test_cases/expected_correct_errorfile
#rm -rf output_correct_dumpfile
#rm -rf output_correct_errorfile
#echo "Fnished testing correct.."

#checking error outputting
#compiler467  -Da ./test_cases/test_incorrect_semantics.c -E output_incorrect_errorfile -U output_incorrect_dumpfile
#diff output_incorrect_errorfile ./test_cases/expected_incorrect_errorfile
#diff output_incorrect_dumpfile ./test_cases/expected_incorrect_dumpfile
#rm -rf output_incorrect_errorfile
#rm -rf output_incorrect_dumpfile
#echo "Fnished testing incorrect.."

make clean
