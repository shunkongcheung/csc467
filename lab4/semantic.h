#ifndef _SEMANTIC_H
#define _SEMANTIC_H

#include "ast.h"

typedef enum{
    SEMANTIC_FAIL,
    SEMANTIC_SUCCESS,
    SEMANTIC_ERROR
}semantic_result;

/* Will check all the syntex in the tree. 
 * Return 1 if okay. 
 * Else return 0
 */
semantic_result semantic_check(node *ast);

/* Various section of semantic check 
 * Return true if the value is check in this function
 */

int checkDivideByZero(node * ast, semantic_result * result);
int checkDeclaration(node * ast, semantic_result * result);
int checkAssignment(node * ast, semantic_result * result);
int checkIndexBoundary(node * ast, semantic_result * result);
int checkFunctionInput(node * ast, semantic_result * result, node_kind arg0, node_kind arg1, char * strarg0kind, char *strarg1kind);
int checkArithmetic(node * ast, semantic_result * result, node_kind arg0, node_kind arg1, char * strarg0kind, char *strarg1kind);
int checkLogic(node * ast, semantic_result * result, node_kind arg0, node_kind arg1, char * strarg0kind, char *strarg1kind);
int checkComparison(node * ast, semantic_result * result, node_kind arg0, node_kind arg1, char * strarg0kind, char *strarg1kind);
int checkConditionIsBool(node * ast, semantic_result * result, node_kind arg0, char * strarg0kind);
int checkVariableAssigned(node * ast);

/* find out the expression type.
 * Assume no error in expression. That is, 
 * if it is a comparison, it just assume it is a bool, 
 * if it is addition, it assume both children is in same type
 * 
 * It does check if a variable is valid as an assignment, 
 * i.e. the flag is a result is invalid
 * 
 * Return NKIND_FAIL only if type is not found
 */
node_kind getExpressionType(node * ast);

/* The return value is the size of valid value
 * if return value is > 0, it indicates at least a value could be found 
 * (if it is a variable, the value is unknown on compile time)
 * The actual value is in the pointer. Assumed a value[4] is supplied
 */
int getExpressionValue(node *ast,  double * value , int * isInt);

/*Helper function for resolving node type*/
void getResolvedNodeType(node_kind, char * result);
#endif