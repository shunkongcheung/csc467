
#include "semantic.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "symbol.h"
#include "common.h"

//TODO: Need to add an indicator where this is in a if scope
int isInIfStatement = 0;

semantic_result semantic_check(node *ast) {

    // should be success if child is null
    // some kind of node may have null child
    if (ast == NULL) {
        return SEMANTIC_SUCCESS;
    }

    // presume result is success.
    semantic_result result = SEMANTIC_SUCCESS;

    // for value testing
    node_kind arg0 = getExpressionType(ast->kids[0]);
    node_kind arg1 = getExpressionType(ast->kids[1]);

    char strarg0kind [20];
    memset(strarg0kind, 0, 20);
    getResolvedNodeType(arg0, strarg0kind);

    char strarg1kind [20];
    memset(strarg1kind, 0, 20);
    getResolvedNodeType(arg1, strarg1kind);

    if (ast->mykind == NKIND_PROGRAM) {
        /* Scope: open scope*/
        if (pushProgramVariables() == 0) {
            fprintf(errorFile, "FAULT: Machine failure in malloc (program)\n");
            return SEMANTIC_ERROR;
        }
    } else if (ast->mykind == NKIND_SCOPE) {
        /* Scope: open scope*/
        if (pushScope() == 0) {
            fprintf(errorFile, "FAULT: Machine failure in malloc (scope)\n");
            return SEMANTIC_ERROR;
        }
    } else if (checkDivideByZero(ast, &result) == 1) {
    }
    // else if ends here... 
    if (checkDeclaration(ast, &result) == 1) {
    } else if (checkAssignment(ast, &result) == 1) {
    } else if (checkIndexBoundary(ast, &result) == 1) {
    } else if (checkFunctionInput(ast, &result, arg0, arg1, strarg0kind, strarg1kind) == 1) {
    } else if (checkArithmetic(ast, &result, arg0, arg1, strarg0kind, strarg1kind) == 1) {
    } else if (checkLogic(ast, &result, arg0, arg1, strarg0kind, strarg1kind) == 1) {
    } else if (checkComparison(ast, &result, arg0, arg1, strarg0kind, strarg1kind) == 1) {
    } else if (checkConditionIsBool(ast, &result, arg0, strarg0kind) == 1) {
    } else if (checkConditionIsBool(ast, &result, arg0, strarg0kind) == 1) {
    } else if (checkVariableAssigned(ast) == 1) {
    }


    /* Done checking this node! traverse to children */
    int childindex = 0;
    for (childindex = 0; childindex < ast->nkids; childindex++) {
        int tmpresult = semantic_check(ast->kids[childindex]);
        if (tmpresult == SEMANTIC_FAIL) result = SEMANTIC_FAIL;
    }

    /* Scope: close scope */
    if (ast->mykind == NKIND_PROGRAM || ast->mykind == NKIND_SCOPE) {
        popVariable();
    }
    if (ast->mykind == NKIND_IF) isInIfStatement -= 1;

    /*Result returning*/
    return result;
}

int checkDivideByZero(node * ast, semantic_result * result) {
    if (ast->mykind == NKIND_EXP_DIVIDE) {
        /*case 1, divided by zero*/
        double value[4];
        int isInt = 0;
        int cancalculate = getExpressionValue(ast->kids[1], value, &isInt);

        // value can be calculated and is a zero!!!
        if (cancalculate > 0 && value[0] == 0) {
            fprintf(errorFile, "ERROR: line %d. Divided by zero error.\n", ast->linenumber);
            *result = SEMANTIC_FAIL;
        }

        return 1;
    }
    return 0;
}

int checkDeclaration(node * ast, semantic_result * result) {

    if (ast->mykind != NKIND_DECLARATION &&
            ast->mykind != NKIND_DECLARE_CONST) {
        return 0;
    }

    // getting variable information
    node_kind type = ast->kids[0]->mykind;

    // there are two cases of declaration:
    // case 1: type identification
    // case 2: type assignment
    char * name = NULL;
    if (ast->kids[1]->mykind == NKIND_IDENT) {
        name = ast->kids[1]->iden;
    } else {
        name = ast->kids[1]->kids[0]->iden;
    }

    /*case 2, duplicated declaration*/
    node_kind found = NKIND_FAIL;
    search_variable scope = getVarKind(name, &found);
    if (scope == SCOPE_FOUND_SAME) {
        char strvarkind [20];
        memset(strvarkind, 0, 20);
        getResolvedNodeType(type, strvarkind);
        fprintf(errorFile, "ERROR: line %d. Duplicated declaration of variable type %s name %s \n", ast->linenumber, strvarkind, name);
        *result = SEMANTIC_FAIL;
    } else {
        // push the variable no matter how
        pushVariable(type, name);

        // set flag for constant
        if (ast->mykind == NKIND_DECLARE_CONST)
            setFlag(name, FLAG_CONST);
    }
    return 1;
}

int checkAssignment(node * ast, semantic_result * result) {
    if (!(ast->mykind == NKIND_ASSIGNMENT_REGULAR ||
            ast->mykind == NKIND_ASSIGNMENT_VEC4 ||
            ast->mykind == NKIND_ASSIGNMENT_VEC3 ||
            ast->mykind == NKIND_ASSIGNMENT_VEC2)) {
        return 0;
    }

    // assigning vector is different from regular:
    // the type of declaration need to change from vec to int/bool/float for comparison
    // there are multiple value to compare instead of just one1
    node_kind varkind = NKIND_FAIL;
    char * name = ast->kids[0]->iden;
    search_variable scope = getVarKind(name, &varkind);
    if (scope == SCOPE_NOTFOUND) {
        fprintf(errorFile, "ERROR: line %d. Assignment to undeclared variable name %s\n", ast->linenumber, name);
        *result = SEMANTIC_FAIL;
        return 1;
    }

    // getting size
    int size = 1;
    if (varkind == NKIND_TYPE_IVEC2 ||
            varkind == NKIND_TYPE_VEC2 ||
            varkind == NKIND_TYPE_BVEC2)
        size = 2;
    else if (varkind == NKIND_TYPE_IVEC3 ||
            varkind == NKIND_TYPE_VEC3 ||
            varkind == NKIND_TYPE_BVEC3)
        size = 3;
    else if (varkind == NKIND_TYPE_IVEC4 ||
            varkind == NKIND_TYPE_VEC4 ||
            varkind == NKIND_TYPE_BVEC4)
        size = 4;


    // change variable type for comparison
    // this change only happen for regular assignment
    if (ast->mykind == NKIND_ASSIGNMENT_REGULAR) {
        switch (varkind) {
            case NKIND_TYPE_INT:varkind = NKIND_INT;
                break;
            case NKIND_TYPE_BOOL:varkind = NKIND_BOOL;
                break;
            case NKIND_TYPE_FLOAT:varkind = NKIND_FLOAT;
                break;
            default:break;
        }
    }// change variable type for comparison. 
        // this change only happen for vector assignment
    else {
        if (varkind == NKIND_TYPE_IVEC2 ||
                varkind == NKIND_TYPE_IVEC3 ||
                varkind == NKIND_TYPE_IVEC4)
            varkind = NKIND_INT;
        else if (varkind == NKIND_TYPE_BVEC2 ||
                varkind == NKIND_TYPE_BVEC3 ||
                varkind == NKIND_TYPE_BVEC4)
            varkind = NKIND_BOOL;
        else if (
                varkind == NKIND_TYPE_VEC2 ||
                varkind == NKIND_TYPE_VEC3 ||
                varkind == NKIND_TYPE_VEC4)
            varkind = NKIND_FLOAT;
        else {
            fprintf(errorFile, "FALUT: line %d. Incompatible parsing to vector assignment..\n", ast->linenumber);
            *result = SEMANTIC_FAIL;
        }
    }

    iden_flag flag;
    getFlag(name, &flag);
    if (flag == FLAG_CONST || flag == FLAG_ATTRIBUTE || flag == FLAG_UNIFORM) {
        /* Assignemnt to constant*/
        fprintf(errorFile, "ERROR: line %d. Re-assigning value to constant %s \n", ast->linenumber, name);
        *result = SEMANTIC_FAIL;
        return 1;
    } else if (isInIfStatement > 0 && flag == FLAG_RESULT) {
        /*Assignment to uniform in If statement*/
        fprintf(errorFile, "ERROR: line %d. Invalid assignment to result variable name %s within an if statement\n", ast->linenumber, name);
        *result = SEMANTIC_FAIL;
    } else if (varkind == NKIND_FAIL) {
        /*Errror with flag*/
        fprintf(errorFile, "ERROR: line %d. Invalid assignment (type mismatch) to variable name %s \n", ast->linenumber, name);
        *result = SEMANTIC_FAIL;
    }

    /*case 3, incorrect assignment*/
    /*case 6, float and integer are incompatible*/
    /*case 7, bool and integer are incompatible*/
    /*case 8, bool and float are incompatible*/
    double value[4];
    int loopvec;
    for (loopvec = 0; loopvec < 4; loopvec++) {

        node_kind expkind = getExpressionType(ast->kids[loopvec + 1]);
        // type mismatch checking
        if (varkind != expkind) {
            char strvarkind [20];
            memset(strvarkind, 0, 20);
            getResolvedNodeType(varkind, strvarkind);

            char strexpkind [20];
            memset(strexpkind, 0, 20);
            getResolvedNodeType(expkind, strexpkind);
            fprintf(errorFile, "ERROR: line %d. Incompatible assignment. Assigning expression of type %s to variable type %s\n",
                    ast->linenumber, strexpkind, strvarkind);
            *result = SEMANTIC_FAIL;
        }

        // this time getting the flag is different from the pervious one
        // since the first escape is already past. This will get the true
        // flag value.
        getFlag(name, &flag);

        // if value can be calculated, then it must be a constant
        int isInt = 0;
        int calculated = getExpressionValue(ast->kids[loopvec + 1], value + loopvec, &isInt);
        if (calculated == 0 && flag == FLAG_CONST) {
            fprintf(errorFile, "ERROR: line %d. Assignment to constant variable name %s is not constant\n", ast->linenumber, name);
            *result = SEMANTIC_FAIL;
        }

        // only do up to required length
        if ((ast->mykind == NKIND_ASSIGNMENT_REGULAR && loopvec == 0) ||
                (ast->mykind == NKIND_ASSIGNMENT_VEC2 && loopvec == 1) ||
                (ast->mykind == NKIND_ASSIGNMENT_VEC3 && loopvec == 2))
            break;
    }

    // value is assigned
    setVarToAssigned(name);

    // bool and int are all integer
    int isInt = 1;
    if (varkind == NKIND_TYPE_FLOAT ||
            varkind == NKIND_TYPE_VEC2 ||
            varkind == NKIND_TYPE_VEC3 ||
            varkind == NKIND_TYPE_VEC4)
        isInt = 0;

    // set the value
    setValue(name, isInt, size, value[0], value[1], value[2], value[3]);

    return 1;

}

int checkIndexBoundary(node * ast, semantic_result * result) {
    if (ast->mykind != NKIND_IDENT_INDEX) {
        return 0;
    }

    node_kind varkind = NKIND_FAIL;
    char * name = ast->kids[0]->iden;
    getVarKind(name, &varkind);
    char strvarkind [20];
    memset(strvarkind, 0, 20);
    getResolvedNodeType(varkind, strvarkind);

    // boundary setting
    int upbound = 0;
    if (varkind == NKIND_TYPE_IVEC2 ||
            varkind == NKIND_TYPE_BVEC2 ||
            varkind == NKIND_TYPE_VEC2)
        upbound = 3;
    else if (varkind == NKIND_TYPE_IVEC3 ||
            varkind == NKIND_TYPE_BVEC3 ||
            varkind == NKIND_TYPE_VEC3)
        upbound = 3;
    else if (
            varkind == NKIND_TYPE_IVEC4 ||
            varkind == NKIND_TYPE_BVEC4 ||
            varkind == NKIND_TYPE_VEC4)
        upbound = 4;
    else {
        fprintf(errorFile, "ERROR: line %d. Invalid indexing to non-vector variable %s of type %s\n", ast->linenumber, name, strvarkind);
        *result = SEMANTIC_FAIL;
    }

    // index checking
    int index = ast->kids[1]->numint;
    if (index < 0 || index >= upbound) {
        fprintf(errorFile, "ERROR: line %d. Invalid index %d to variable type %s\n", ast->linenumber, index, strvarkind);
        *result = SEMANTIC_FAIL;
    }

    return 1;
}

int checkFunctionInput(node * ast, semantic_result * result, node_kind arg0, node_kind arg1, char * strarg0kind, char *strarg1kind) {
    if (ast->mykind == NKIND_FUN_DP3) {

        if (arg0 != arg1 ||
                (arg0 != NKIND_TYPE_VEC3 && arg0 != NKIND_TYPE_VEC4 &&
                arg0 != NKIND_TYPE_IVEC3 && arg0 != NKIND_TYPE_IVEC4)) {
            /* Function: DP3 only accept certain types */
            fprintf(errorFile, "ERROR: line %d. Invalid argument to function DP3 type %s and %s\n", ast->linenumber, strarg0kind, strarg1kind);
            *result = SEMANTIC_FAIL;
        }
        return 1;
    } else if (ast->mykind == NKIND_FUN_RSQ) {
        if (arg0 != NKIND_INT && arg0 != NKIND_FLOAT) {
            /* Function: RSK only accept int and float as input */
            fprintf(errorFile, "ERROR: line %d. Invalid argument to function RSQ type %s\n", ast->linenumber, strarg0kind);
            *result = SEMANTIC_FAIL;
        }
        return 1;
    } else if (ast->mykind == NKIND_FUN_LIT) {
        if (arg0 != NKIND_TYPE_VEC4) {
            /* Function: nput type can only be a float or int */
            fprintf(errorFile, "ERROR: line %d. Invalid argument to function LIT type %s\n", ast->linenumber, strarg0kind);
            *result = SEMANTIC_FAIL;
        }
        return 1;
    }
    return 0;
}

int checkArithmetic(node * ast, semantic_result * result, node_kind arg0, node_kind arg1, char * strarg0kind, char *strarg1kind) {
    if (ast->mykind == NKIND_EXP_PLUS ||
            ast->mykind == NKIND_EXP_MINUS) {
        /* Expression: accept all type, but must be matching*/
        if (arg0 != arg1) {
            fprintf(errorFile, "ERROR: line %d. +- Expression having type mismatch %s and %s \n", ast->linenumber, strarg0kind, strarg1kind);
            *result = SEMANTIC_FAIL;
        }
        return 1;
    } else if (ast->mykind == NKIND_EXP_MULTI) {
        // change a vector to a scalar when considering
        if (arg0 == NKIND_TYPE_IVEC2 ||
                arg0 == NKIND_TYPE_IVEC3 ||
                arg0 == NKIND_TYPE_IVEC4)
            arg0 = NKIND_INT;
//        else if (arg0 == NKIND_TYPE_BVEC2 ||
//                arg0 == NKIND_TYPE_BVEC3 ||
//                arg0 == NKIND_TYPE_BVEC4)
//            arg0 = NKIND_BOOL;
        else if (
                arg0 == NKIND_TYPE_VEC2 ||
                arg0 == NKIND_TYPE_VEC3 ||
                arg0 == NKIND_TYPE_VEC4)
            arg0 = NKIND_FLOAT;

        if (arg1 == NKIND_TYPE_IVEC2 ||
                arg1 == NKIND_TYPE_IVEC3 ||
                arg1 == NKIND_TYPE_IVEC4)
            arg1 = NKIND_INT;
//        else if (arg1 == NKIND_TYPE_BVEC2 ||
//                arg1 == NKIND_TYPE_BVEC3 ||
//                arg1 == NKIND_TYPE_BVEC4)
//            arg1 = NKIND_BOOL;
        else if (
                arg1 == NKIND_TYPE_VEC2 ||
                arg1 == NKIND_TYPE_VEC3 ||
                arg1 == NKIND_TYPE_VEC4)
            arg1 = NKIND_FLOAT;

        // two type must match
        if (arg0 != arg1) {
            fprintf(errorFile, "ERROR: line %d. * Expression having type mismatch %s and %s \n", ast->linenumber, strarg0kind, strarg1kind);
            *result = SEMANTIC_FAIL;
        }
        return 1;
    } else if (ast->mykind == NKIND_EXP_EXPO ||
            ast->mykind == NKIND_EXP_DIVIDE) {
        /* Expression: accept only two type and must be matching*/
        if (arg0 != NKIND_INT && arg0 != NKIND_FLOAT) {
            fprintf(errorFile, "ERROR: line %d. /^ Expression having invalid type %s\n", ast->linenumber, strarg0kind);
            *result = SEMANTIC_FAIL;
        } else if (arg0 != arg1) {
            fprintf(errorFile, "ERROR: line %d. /^ Expression having type mismatch %s and %s \n", ast->linenumber, strarg0kind, strarg1kind);
            *result = SEMANTIC_FAIL;
        }
        return 1;
    }
    return 0;
}

int checkLogic(node * ast, semantic_result * result, node_kind arg0, node_kind arg1, char * strarg0kind, char *strarg1kind) {
    if (ast->mykind == NKIND_EXP_AND ||
            ast->mykind == NKIND_EXP_OR) {
        // EXPRESSION: operation can only be done on bool
        if ((arg0 != NKIND_BOOL && arg0 != NKIND_TYPE_BVEC2 && arg0 != NKIND_TYPE_BVEC3 && arg0 != NKIND_TYPE_BVEC4) ||
                (arg1 != NKIND_BOOL && arg1 != NKIND_TYPE_BVEC2 && arg1 != NKIND_TYPE_BVEC3 && arg1 != NKIND_TYPE_BVEC4)) {
            fprintf(errorFile, "ERROR: line %d. && || Expression having invalid type %s and %s \n", ast->linenumber, strarg0kind, strarg1kind);
            *result = SEMANTIC_FAIL;
        }
        return 1;
    } else if (ast->mykind == NKIND_EXP_NOT) {
        if (arg0 != NKIND_BOOL && arg0 != NKIND_TYPE_BVEC2 && arg0 != NKIND_TYPE_BVEC3 && arg0 != NKIND_TYPE_BVEC4) {
            fprintf(errorFile, "ERROR: line %d. ! Expression having invalid type %s \n", ast->linenumber, strarg0kind);
            *result = SEMANTIC_FAIL;
        }
        return 1;
    }
    return 0;
}

int checkComparison(node * ast, semantic_result * result, node_kind arg0, node_kind arg1, char * strarg0kind, char *strarg1kind) {
    if (ast->mykind != NKIND_EXP_COMP) {
        return 0;
    }
    /* Expression: must be matching*/
    node_kind arg2 = getExpressionType(ast->kids[2]);
    char strarg2kind [20];
    memset(strarg2kind, 0, 20);
    getResolvedNodeType(arg2, strarg2kind);

    // get back the original type
    arg1 = ast->kids[1]->mykind;

    // for any comparison other than (== and !=)
    // only perform on scalar
    if (arg1 != NKIND_COMP_EQUAL && arg1 != NKIND_COMP_NOT_EQUAL &&
            arg0 != NKIND_INT && arg0 != NKIND_FLOAT) {
        fprintf(errorFile, "ERROR: line %d. < <= > >= Expression having invalid type %s\n", ast->linenumber, strarg0kind);
        *result = SEMANTIC_FAIL;
    }
    // argument type must match
    if (arg0 != arg2) {
        fprintf(errorFile, "ERROR: line %d. Operate Expression having type mismatch %s and %s \n", ast->linenumber, strarg0kind, strarg2kind);
        *result = SEMANTIC_FAIL;
    }
    return 1;

}

int checkConditionIsBool(node * ast, semantic_result * result, node_kind arg0, char * strarg0kind) {
    if (ast->mykind == NKIND_IF ||
            ast->mykind == NKIND_LOOP) {
        /* If, While: Checking condition is a boolean*/
        if (arg0 != NKIND_BOOL) {
            fprintf(errorFile, "ERROR: line %d. Condition should be a bool but is now type %s \n", ast->linenumber, strarg0kind);
            *result = SEMANTIC_FAIL;
        }
        if (ast->mykind == NKIND_IF) isInIfStatement += 1;

        return 1;
    }
    return 0;
}

int checkVariableAssigned(node * ast) {
    if (ast->mykind == NKIND_IDENT) {
        /* Checking if variable is assigned*/
        char * name = ast->iden;
        int assigned = getVarAssigned(name);
        if (!assigned) {
            fprintf(errorFile, "WARNING: line %d. Using unassigned variable %s\n", ast->linenumber, name);
        }
    }
    return 1;
}

node_kind getExpressionType(node * ast) {
    if (ast == NULL) return NKIND_FAIL;

    // it is a value already
    if (ast->mykind == NKIND_INT ||
            ast->mykind == NKIND_FLOAT ||
            ast->mykind == NKIND_BOOL) {
        return ast->mykind;

    }// if it is a variable, look for its type
    else if (ast->mykind == NKIND_IDENT) {

        // a result variable could not be in an expression
        iden_flag flag;
        getFlag(ast->iden, &flag);
        if (flag == FLAG_RESULT) {
            return NKIND_FAIL;
        }

        node_kind type = NKIND_FAIL;
        getVarKind(ast->iden, &type);
        switch (type) {
            case NKIND_TYPE_INT:return NKIND_INT;
            case NKIND_TYPE_BOOL:return NKIND_BOOL;
            case NKIND_TYPE_FLOAT:return NKIND_FLOAT;

                // here should not return -1. 
                // because it may be a vector type
            default: return type;
        }

    }// note that there is a difference 
    else if (ast->mykind == NKIND_IDENT_INDEX) {

        node_kind type = NKIND_FAIL;
        getVarKind(ast->kids[0]->iden, &type);
        switch (type) {
            case NKIND_TYPE_IVEC2:
            case NKIND_TYPE_IVEC3:
            case NKIND_TYPE_IVEC4: return NKIND_INT;

            case NKIND_TYPE_VEC2:
            case NKIND_TYPE_VEC3:
            case NKIND_TYPE_VEC4: return NKIND_FLOAT;

            case NKIND_TYPE_BVEC2:
            case NKIND_TYPE_BVEC3:
            case NKIND_TYPE_BVEC4: return NKIND_BOOL;

                // indexing only support the above type
                // if unresolved, then it is an error.
            default: return NKIND_FAIL;
        }
    } else if (ast->mykind == NKIND_FUN_RSQ) {
        /* functions. Assume the input is correct for here.
         will check if input is correct later*/
        return NKIND_FLOAT;

    } else if (ast->mykind == NKIND_FUN_DP3) {
        /* functions. Assume the input is correct for here.
        will check if input is correct later*/
        if (getExpressionType(ast->kids[0]) == NKIND_TYPE_VEC4 ||
                getExpressionType(ast->kids[0]) == NKIND_TYPE_VEC3)
            return NKIND_FLOAT;
        else
            return NKIND_INT;

    } else if (ast->mykind == NKIND_FUN_LIT) {
        /* functions. Assume the input is correct for here.
        will check if input is correct later*/
        return NKIND_TYPE_VEC4;

    } else if (ast->mykind == NKIND_EXP_PLUS ||
            ast->mykind == NKIND_EXP_MINUS ||
            ast->mykind == NKIND_EXP_DIVIDE ||
            ast->mykind == NKIND_EXP_EXPO ||
            ast->mykind == NKIND_EXP_NEG) {
        // if it is an expression: operation
        // assume two of the input are matching, so just return the first one
        return getExpressionType(ast->kids[0]);

    } else if (ast->mykind == NKIND_EXP_MULTI) {
        // for multiply, since it is possible to have scale * vector
        // it is necessary to see both side of the type. 
        // if there is a vector, return the vector type
        node_kind left = getExpressionType(ast->kids[0]);
        node_kind right = getExpressionType(ast->kids[1]);

        if (left == NKIND_TYPE_IVEC2 ||
                left == NKIND_TYPE_IVEC3 ||
                left == NKIND_TYPE_IVEC4 ||

                left == NKIND_TYPE_VEC2 ||
                left == NKIND_TYPE_VEC3 ||
                left == NKIND_TYPE_VEC4 ||

                left == NKIND_TYPE_BVEC2 ||
                left == NKIND_TYPE_BVEC3 ||
                left == NKIND_TYPE_BVEC4)
            return left;
        else
            return right;

    } else if (ast->mykind == NKIND_EXP_COMP ||
            ast->mykind == NKIND_EXP_AND ||
            ast->mykind == NKIND_EXP_OR ||
            ast->mykind == NKIND_EXP_NOT) {
        // all there would return a boolean type
        return NKIND_BOOL;

    } else if (ast->mykind == NKIND_EXP_CAST) {
        // find the expression type first
        node_kind casttype = ast->kids[0]->mykind;
        node_kind exptype = getExpressionType(ast->kids[1]);

        // only support a few type to be cast
        if (exptype != NKIND_INT &&
                exptype != NKIND_BOOL &&
                exptype != NKIND_FLOAT)
            return NKIND_FAIL;

        // only support a few type of casting
        switch (casttype) {
            case NKIND_TYPE_INT: return NKIND_INT;
            case NKIND_TYPE_BOOL: return NKIND_BOOL;
            case NKIND_TYPE_FLOAT: return NKIND_FLOAT;
            default: return NKIND_FAIL;
        }
    }
    // unresolved type ..
    return NKIND_FAIL;
}

int getExpressionValue(node *ast, double * value, int * isInt) {
    if (ast == NULL)return 0;

    // it is a value already
    if (ast->mykind == NKIND_INT) {
        *value = ast->numint;
        *isInt = 1;
        return 1;
    } else if (ast->mykind == NKIND_FLOAT) {
        *value = ast->numfloat;
        *isInt = 0;
        return 1;
    } else if (ast->mykind == NKIND_IDENT) {
        // get flag
        iden_flag flag;
        getFlag(ast->iden, &flag);

        // a certain number
        if (flag == FLAG_CONST || flag == FLAG_UNIFORM) {
            int size = 0;
            float * arr = getValue(ast->iden, &size);
            printf("hello : %d %s\n", size, ast->iden);
            // copy all value in array to value
            int i;
            for (i = 0; i < size; i++)
                value[i] = arr[i];

            // return the size of array
            return size;
        }
        return 0;
    } else if (ast->mykind == NKIND_IDENT_INDEX) {

        char * name = ast->kids[0]->iden;

        // get flag
        iden_flag flag;
        getFlag(name, &flag);

        if (flag == FLAG_CONST || flag == FLAG_UNIFORM) {
            // indexed, therefore only size of one

            int index = ast->kids[1]->numint;

            int size = 0;
            float * arr = getValue(name, &size);

            // if required index is incorrect
            if (size <= index) return 0;

            // required index is correct
            *value = arr[index];

            return 1;
        }
        return 0;
    } else if (ast->mykind == NKIND_EXP_CAST) {
        node_kind casttype = ast->kids[0]->mykind;
        int count = getExpressionValue(ast->kids[1], value, isInt);
        *isInt = (casttype == NKIND_TYPE_INT || casttype == NKIND_TYPE_BOOL) ? 1 : 0;
        return count;
    }

    // if it is a expression, need to recursively call
    double leftvalue[4];
    int leftIsInt = 0;
    int leftsize = getExpressionValue(ast->kids[0], leftvalue, &leftIsInt);

    double rightvalue[4];
    int rightIsInt = 0;
    int rightsize = getExpressionValue(ast->kids[1], rightvalue, &rightIsInt);

    // calculate the values,
    int i;
    int smallersize = (leftsize > rightsize) ? rightsize : leftsize;
    switch (ast->mykind) {
        case NKIND_EXP_PLUS:
            for (i = 0; i < smallersize; i++)
                value[i] = leftvalue[i] + rightvalue[i];
            break;
        case NKIND_EXP_MINUS:
            for (i = 0; i < smallersize; i++)
                value[i] = leftvalue[i] - rightvalue[i];
            break;

        case NKIND_EXP_EXPO:
            // only support scalar exponential
            value[0] = pow(leftvalue[0], rightvalue[0]);
            break;

        case NKIND_EXP_MULTI:

            if (leftsize == 1) {
                // scalar * scalar/vector
                for (i = 0; i < smallersize; i++) {
                    if (leftIsInt == 1)
                        value[i] = (int) leftvalue[0] * (int) rightvalue[i];
                    else
                        value[i] = leftvalue[0] * rightvalue[i];
                }
            } else if (rightsize == 1) {
                // scalar/vector * scalar
                for (i = 0; i < smallersize; i++) {
                    if (leftIsInt == 1)
                        value[i] = (int) leftvalue[i] * (int) rightvalue[0];
                    else
                        value[i] = leftvalue[i] * rightvalue[0];
                }
            }
            else {
                // vector * vector
                for (i = 0; i < smallersize; i++) {
                    if (leftIsInt == 1)
                        value[i] = (int) leftvalue[i] * (int) rightvalue[i];
                    else
                        value[i] = leftvalue[i] * rightvalue[i];
                }
            }
            break;

        case NKIND_EXP_DIVIDE:
            // only support scalar division

            // special case. where this divide is invalid already. return for now
            if (rightvalue[0] == 0) {
                *value = 0;
                return 1;
            }
            if (leftIsInt == 1)
                value[0] = (int) leftvalue[0] / (int) rightvalue[0];
            else
                value[0] = leftvalue[0] / rightvalue[0];
            break;

        case NKIND_EXP_NEG:
            // support scalar and vector
            for (i = 0; i < smallersize; i++)
                value[i] = -leftvalue[i];
            break;

        default:
            // invalid case
            smallersize = 0;
            break;
    }

    // tell the above layer if this value is going to be an integer
    *isInt = leftIsInt;

    // return the size of this result
    return smallersize;
}

void getResolvedNodeType(node_kind kind, char * result) {
    switch (kind) {
        case NKIND_FAIL: strcpy(result, "NKIND_FAIL");
            //            break;
            //
            //        case NKIND_SCOPE: strcpy(result, "NKIND_SCOPE");
            //            break;
            //        case NKIND_INSTRUCTIONS: strcpy(result, "NKIND_INSTRUCTIONS");
            //            break;
            //        case NKIND_INDEX: strcpy(result, "NKIND_INDEX");
            //            break;
            //        case NKIND_DECLARATION: strcpy(result, "NKIND_DECLARATION");
            //            break;
            //
            //        case NKIND_ASSIGNMENT_REGULAR: strcpy(result, "NKIND_ASSIGNMENT_REGULAR");
            //            break;
            //
            //        case NKIND_ASSIGNMENT_INDEX: strcpy(result, "assign index");
            //            break;
            //        case NKIND_ASSIGNMENT_VEC2: strcpy(result, "assign vec2");
            //            break;
            //        case NKIND_ASSIGNMENT_VEC3: strcpy(result, "assign vec3");
            //            break;
            //        case NKIND_ASSIGNMENT_VEC4: strcpy(result, "assign vec4");
            //            break;

        case NKIND_IF: strcpy(result, "NKIND_IF");
            break;
        case NKIND_EIF: strcpy(result, "NKIND_EIF");
            break;
        case NKIND_ELSE: strcpy(result, "NKIND_ELSE");
            break;
        case NKIND_STATEMENT: strcpy(result, "NKIND_STATEMENT");
            break;
        case NKIND_LOOP: strcpy(result, "NKIND_LOOP");
            break;

        case NKIND_INT: strcpy(result, "int");
            break;
        case NKIND_FLOAT: strcpy(result, "float");
            break;
        case NKIND_BOOL: strcpy(result, "bool");
            break;
        case NKIND_IDENT_INDEX: strcpy(result, "NKIND_IDENT_INDEX");
            break;
        case NKIND_IDENT: strcpy(result, "NKIND_IDENT");
            break;

        case NKIND_TYPE_INT: strcpy(result, "int");
            break;
        case NKIND_TYPE_BOOL: strcpy(result, "bool");
            break;
        case NKIND_TYPE_FLOAT: strcpy(result, "float");
            break;

        case NKIND_TYPE_BVEC2: strcpy(result, "bvec2");
            break;
        case NKIND_TYPE_BVEC3: strcpy(result, "bvec3");
            break;
        case NKIND_TYPE_BVEC4: strcpy(result, "bvec4");
            break;

        case NKIND_TYPE_IVEC2: strcpy(result, "ibvec2");
            break;
        case NKIND_TYPE_IVEC3: strcpy(result, "ibvec3");
            break;
        case NKIND_TYPE_IVEC4: strcpy(result, "ibvec4");
            break;

        case NKIND_TYPE_VEC2: strcpy(result, "vec2");
            break;
        case NKIND_TYPE_VEC3: strcpy(result, "vec3");
            break;
        case NKIND_TYPE_VEC4: strcpy(result, "vec4");
            break;

        case NKIND_EXP_NEG: strcpy(result, "-");
            break;
        case NKIND_EXP_NOT: strcpy(result, "!");
            break;

        case NKIND_EXP_PLUS: strcpy(result, "-");
            break;
        case NKIND_EXP_MINUS: strcpy(result, "!");
            break;
        case NKIND_EXP_MULTI: strcpy(result, "-");
            break;
        case NKIND_EXP_DIVIDE: strcpy(result, "!");
            break;
        case NKIND_EXP_EXPO: strcpy(result, "-");
            break;
        case NKIND_EXP_AND: strcpy(result, "&&");
            break;
        case NKIND_EXP_OR: strcpy(result, "||");
            break;


        case NKIND_COMP_EQUAL: strcpy(result, "==");
            break;
        case NKIND_COMP_NOT_EQUAL: strcpy(result, "!=");
            break;
        case NKIND_COMP_GREATER_EQUAL: strcpy(result, ">=");
            break;
        case NKIND_COMP_LESS_EQUAL: strcpy(result, "<=");
            break;
        case NKIND_COMP_GREATER: strcpy(result, ">");
            break;
        case NKIND_COMP_LESS: strcpy(result, "<");
            break;

        default: strcpy(result, "NOT FOUND");
            break;
    }
    return;
}