{
    /* declaration and casting of all types */
    int myint0;
    int myint1 = 1;
    int myint2 = (int)2.0;

    float myflo0;
    float myflo1 = 2.0;
    float myflo2 = (float) 2;

    bool mybool0;
    bool mybool1 = true;
    bool mybool2 = (bool) 1;

    vec2 vec_2 = vec2(1.0, 2.0);
    vec3 vec_3 = vec3(1.0, 2.0, 3.0);
    vec4 vec_4 = vec4(1.0, 2.0, 3.0, (float)4 );

    ivec2 ivec_2 = ivec2(1, 2);
    ivec3 ivec_3 = ivec3(1, 2, 3);
    ivec4 ivec_4 = ivec4(1, 2, 3, (int)4.0 );

    bvec2 bvec_2 = bvec2(true, false);
    bvec3 bvec_3 = bvec3(true, false, false);
    bvec4 bvec_4 = bvec4(true, false, true, (bool)1 );/*TODO: change syntex*/

    /* Assignment and all kind of expression */
    myint0 = 0;
    myint1 = 1 + 1;
    myint2 = (2*4)^(4/3) - 6*( myint1 + myint0);
    myflo0 = ((float)myint0) + ((float)mybool1);

    /* NEG to scalar and vector */
    myint0 = -myint0;
    ivec_2 = -ivec_2;

    /* plus and minus to scalar and vector */
    myint0 = myint0 + myint0;
    myint0 = myint0 - myint0;

    ivec_2 = ivec_2 + ivec_2;
    ivec_2 = ivec_2 - ivec_2;

    /* multiply to scalar and vector */
    /* also checking indexing */
    ivec_2 = myint0 * ivec_2;
    ivec_2 = ivec_2 * ivec_2;
    myint0 = myint0 * myint0;

    /* divide and expo to scalar and vector */
    myint0 = myint0 / myint0;
    myflo0 = myflo0 ^ myflo0;

    /* Logical can only be done on bool */
    /* && || ! can be done on both vector and scalar */
    if( !bvec_4[0] && !mybool2 || mybool1){

        /* comparison can only be done on scalar*/
        if( myflo0 > myflo1){}
        else if( myflo0 < myflo1){}
        else if( myflo0 >= myflo1){}
        else if( myflo0 <= myflo1){}

        /* Comparsion that can done on both vector and scalar */
        if( myflo0 == myflo1){}
        if (myflo0 != myflo1){}
        if( ivec_2 == ivec_2){}
        if( ivec_2 != ivec_2){}

        /* redeclare of variable in different scope*/
        int myint0;
    }
    else if(true){}

    /* use of predefined function */
    float fun_rsq_f = rsq(1.0);
    float fun_rsq_i = rsq(1);
    
    float fun_dp3_f = dp3(vec_4, vec_4);
    fun_dp3_f = dp3(vec_3, vec_3);
    
    int fun_dp3_i = dp3(ivec_4, ivec_4);
    fun_dp3_i = dp3(ivec_3, ivec_3);
    
    vec4 lit_vec4 = lit(vec_4);

    /* indexing */
    myint0 = ivec_2[0] + ivec_2[1];

    /* Use of predefined variable and constant */
    const int myconst = (int)gl_Light_Half[2];
    gl_FragColor = lit_vec4;
    
    const vec4 myconstvec4 = gl_Light_Half;
    const ivec2 myconstivec2 = ivec2(1, 2);
    

}

