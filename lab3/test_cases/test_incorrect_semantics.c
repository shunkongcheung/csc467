{
    /* declaration and casting of all types */
    int myint0;
    int myint1 = 1;
    int myint2 = 2.0;

    float myflo0;
    float myflo1 = 2.0;
    float myflo2 = 2;

    bool mybool0;
    bool mybool1 = true;
    bool mybool2 = 1;

    vec2 vec_2 = {1.0, 2.0};
    vec3 vec_3 = {1.0, 2.0, 3.0};
    vec4 vec_4 = {1.0, 2.0, 3.0, 4 };/*no casting, in valid declaration*/

    ivec2 ivec_2 = {1, 2};
    ivec3 ivec_3 = {1, 2, 3};
    ivec4 ivec_4 = {1, 2, 3, 4.0 };/*no casting, in valid declaration*/

    bvec2 bvec_2 = {true, false};
    bvec3 bvec_3 = {true, false, false};
    bvec4 bvec_4 = {true, false, true, 1 };/*no casting, in valid declaration*/

    /*no casting, in valid declaration*/
    myflo0 = myint0 + mybool1;

    /* multiply a scalar and vector should produce a vector */
    myint0 = myint1 * ivec_2;

    /* Logical can only be done on bool */
    /* && || ! can be done on both vector and scalar */
    if( !ivec_3[0] && !myint1 || myflo1){

        /* comparison can only be done on scalar*/
        if( ivec_2 > ivec_2){}
        if( ivec_2 < ivec_2){}
        if( ivec_2 >= ivec_2){}
        if( ivec_2 <= ivec_2){}

        /* redeclare of variable in different scope*/
        int insider;
        
        /* Cannot assign result in a if statement*/
        if(true){
            gl_FragColor = vec_3;
        }
		
		/* Using unassigned value */
		vec3 unassigned_vec3;
		vec_3 = unassigned_vec3;
    }
    insider = 0;/*different scope*/

    /* use of predefined function */
    float fun_rsq_f = rsq(1);
    int fun_rsq_i = rsq(1.0);
    
    float fun_dp3_f = dp3(vec_4, ivec_4);
    vec4 lit_vec4 = lit(ivec_4);

    /* incorrect indexing */
    myint0 = ivec_2[-1] + ivec_2[2] + (int)bvec_3[3] + (int)vec_4[4];

    /* redefining and using variable to define constant*/
    const int mywrongconst = myint1;
    const int anotherwrongconst = 3;
    anotherwrongconst = 3;
    
    /* invalid use predefined variable */
    myint0 = gl_FragColor[0];
    gl_TexCoord = vec_4;
    gl_Light_Half = vec_4;
    const float thirdwrongconst = gl_TexCoord[0];
    

}
