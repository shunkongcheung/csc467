#include "ast.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "common.h"
#include "symbol.h"
#include "semantic.h"

struct node *ast = NULL;

void preorder(node * currnode, void(*f)(struct node *)) {

    if (currnode == NULL)return;
    // call myself before calling child

    f(currnode);

    // call the childs first
    int i;
    for (i = 0; i < currnode->nkids; i++)
        preorder(currnode->kids[i], f);

}

void postorder(node * currnode, void(*f)(struct node *)) {
    if (currnode == NULL)return;

    // call the childs first
    int i;
    for (i = 0; i < currnode->nkids; i++)
        postorder(currnode->kids[i], f);

    // call myself after calling childs
    f(currnode);
}

struct node *ast_allocate(node_kind mykind, int linenum, int kidcount, ...) {
    // should have at least one kid
    if (kidcount < 0) {
        fprintf(errorFile, "ast_allocate: kind count is smaller than zero (%d)\n", kidcount);
        return NULL;
    }

    // counting extra child needed
    int extrachild = (kidcount - 1);
    if (extrachild < 0) extrachild = 0;

    // dynamic allocate memory for node
    struct node * ptr = (struct node*) malloc(sizeof (struct node) + sizeof (struct node*)*extrachild);
    if (ptr == NULL) {
        fprintf(errorFile, "ast_allocate: Could not dynmaic allocate memory for new node\n");
        return NULL;
    }
    memset(ptr, 0, sizeof (struct node) + sizeof (struct node*)*extrachild);

    va_list ap;
    ptr->mykind = mykind;
    ptr->linenumber = linenum;
    ptr->nkids = kidcount;
    va_start(ap, kidcount);


    // node value.
    if (mykind == NKIND_INT ||
            mykind == NKIND_INDEX ||
            mykind == NKIND_BOOL)
        ptr->numint = va_arg(ap, int);
    else if (mykind == NKIND_FLOAT)
        ptr->numfloat = (float) va_arg(ap, double);
    else if (mykind == NKIND_IDENT) {
        // need to deep copy?
        char * tmp = va_arg(ap, char*);
        ptr->iden = (char*) malloc(sizeof (char)*strlen(tmp) + 1);
        memcpy(ptr->iden, tmp, strlen(tmp) + 1);
    }

    // successfully created Now assign child pointer
    int i;
    for (i = 0; i < kidcount; i++)
        ptr->kids[i] = va_arg(ap, struct node*);
    va_end(ap);

    // result is found. return
    return ptr;
}

void myFree(struct node *root) {
    free(root);
}

void ast_free(struct node * root) {
    postorder(root, myFree);
}

void ast_print(struct node * ast, int *scopelevel, int lastprintline) {

    if (ast == NULL) return;
    int myline = ast->linenumber;
    int traverse = 1;
    int indent = 1;
    int loopchild = 0;

    // value to be print
    char printing[80];
    memset(printing, 0, 80);

    if (ast->mykind == NKIND_PROGRAM) {
        /* Scope: open scope*/
        if (pushProgramVariables() == 0) {
            fprintf(errorFile, "FAULT: Machine failure in malloc (program)\n");
            return;
        }
    } else if (ast->mykind == NKIND_SCOPE) {

        /* Scope: open scope*/
        if (pushScope() == 0) {
            fprintf(errorFile, "FAULT: Machine failure in malloc (scope)\n");
            return;
        }
        // printing scope
        sprintf(printing, "(SCOPE");
        *scopelevel = *scopelevel + 1;

    } else if (ast->mykind == NKIND_DECLARATION ||
            ast->mykind == NKIND_DECLARE_CONST) {

        // name 
        char * name = NULL;
        if (ast->kids[1]->mykind == NKIND_IDENT) {
            name = ast->kids[1]->iden;
        } else {
            name = ast->kids[1]->kids[0]->iden;
        }

        // type
        node_kind type = ast->kids[0]->mykind;

        // push the variable no matter how
        pushVariable(type, name);

        char strtype [20];
        memset(strtype, 0, 20);
        getResolvedNodeType(type, strtype);

        // the value is known for the assignment
        if (ast->mykind == NKIND_DECLARE_CONST) {

            setFlag(name, FLAG_CONST);

            double value[4];
            int isInt = (type == NKIND_TYPE_INT || type == NKIND_TYPE_BOOL
                    || type == NKIND_TYPE_BVEC2 || type == NKIND_TYPE_BVEC3
                    || type == NKIND_TYPE_BVEC4 || type == NKIND_TYPE_IVEC2
                    || type == NKIND_TYPE_IVEC3 || type == NKIND_TYPE_IVEC4);
            int size = 0;
            if (ast->kids[1]->nkids > 2) {

                size = ast->kids[1]->nkids;
                for (int i = 1; i < size; i++) {
                    if (isInt)
                        value[i - 1] = (double) ast->kids[1]->kids[i]->numint;
                    else {
                        value[i - 1] = (double) ast->kids[1]->kids[i]->numfloat;
                    }
                }
                // kids[0] is identifier, kids[1] is {1,}..
                size--;
                setValue(name, size, isInt, value[0], value[1], value[2], value[3]);

            } else
                size = getExpressionValue(ast->kids[1]->kids[1], value, &isInt);
            printf("size = %d\n", size);

            sprintf(printing, "(DECLARATION const %s %s ", strtype, name);
            printf("Hihi %s\n", printing);
            int i;
            for (i = 0; i < size; i++)
                if (isInt)
                    sprintf(printing + strlen(printing), "%d ", (int) value[i]);
                else
                    sprintf(printing + strlen(printing), "%5.3lf ", value[i]);

            traverse = 0;
        } else {
            sprintf(printing, "(DECLARATION %s %s)", strtype, name);
            if (ast->kids[1]->mykind == NKIND_IDENT)
                traverse = 0;

        }

    } else if (ast->mykind == NKIND_ASSIGNMENT_REGULAR ||
            ast->mykind == NKIND_ASSIGNMENT_VEC4 ||
            ast->mykind == NKIND_ASSIGNMENT_VEC3 ||
            ast->mykind == NKIND_ASSIGNMENT_VEC2) {

        node_kind varkind = NKIND_FAIL;
        char * name = ast->kids[0]->iden;
        getVarKind(name, &varkind);

        char strtype [20];
        memset(strtype, 0, 20);
        getResolvedNodeType(varkind, strtype);

        sprintf(printing, "(ASSIGN %s %s ", strtype, name);
        loopchild = 1;
    } else if (ast->mykind == NKIND_IF) {
        sprintf(printing, "(IF ");
    } else if (ast->mykind == NKIND_EIF) {
        sprintf(printing, "(EIF ");
    } else if (ast->mykind == NKIND_EXP_NEG ||
            ast->mykind == NKIND_EXP_NOT) {

        node_kind varkind = getExpressionType(ast->kids[0]);

        char strtype [20];
        memset(strtype, 0, 20);
        getResolvedNodeType(varkind, strtype);

        char opttype [20];
        memset(opttype, 0, 20);
        getResolvedNodeType(ast->mykind, opttype);

        sprintf(printing, "(UNARY %s %s ", opttype, strtype);
        indent = 0;

    } else if (ast->mykind == NKIND_EXP_PLUS ||
            ast->mykind == NKIND_EXP_MINUS ||
            ast->mykind == NKIND_EXP_MULTI ||
            ast->mykind == NKIND_EXP_DIVIDE ||
            ast->mykind == NKIND_EXP_EXPO ||

            ast->mykind == NKIND_EXP_AND ||
            ast->mykind == NKIND_EXP_OR
            ) {

        node_kind varkind = getExpressionType(ast->kids[0]);

        char strtype [20];
        memset(strtype, 0, 20);
        getResolvedNodeType(varkind, strtype);

        char opttype [20];
        memset(opttype, 0, 20);
        getResolvedNodeType(ast->mykind, opttype);

        sprintf(printing, "(BINARY %s %s ", opttype, strtype);
        indent = 0;

    } else if (ast->mykind == NKIND_EXP_COMP) {

        char strtype [20];
        memset(strtype, 0, 20);
        getResolvedNodeType(ast->kids[1]->mykind, strtype);

        sprintf(printing, "(BINARY bool %s ", strtype);
        indent = 0;


    } else if (ast->mykind == NKIND_INT) {
        sprintf(printing, "%d ", ast->numint);
        indent = 0;

    } else if (ast->mykind == NKIND_FLOAT) {
        sprintf(printing, "%5.3lf ", ast->numfloat);
        indent = 0;

    } else if (ast->mykind == NKIND_BOOL) {
        sprintf(printing, (ast->numint == 1) ? "true " : "false ");
        indent = 0;

    } else if (ast->mykind == NKIND_IDENT) {
        // if it is a constant, print out the value. 
        iden_flag varflag = FLAG_REGULAR;
        char * name = ast->iden;
        getFlag(name, &varflag);

        node_kind varkind = getExpressionType(ast->kids[0]);

        if (varflag == FLAG_UNIFORM ||
                varflag == FLAG_CONST ||
                varflag == FLAG_ATTRIBUTE) {
            float *value;
            int size;
            value = getValue(name, &size);

            // print all value
            int i;
            for (i = 0; i < size; i++)
                if (varkind == NKIND_INT)
                    sprintf(printing + strlen(printing), "%d ", (int) value[i]);
                else
                    sprintf(printing + strlen(printing), "%5.3lf ", value[i]);

        } else {
            // not a constant, print the name
            sprintf(printing, "%s  ", name);
        }

        indent = 0;

    } else if (ast->mykind == NKIND_EXP_CAST) {
        char strtype [20];
        memset(strtype, 0, 20);
        getResolvedNodeType(ast->kids[0]->mykind, strtype);

        sprintf(printing, "(%s)", strtype);
        indent = 0;

    } else if (ast->mykind == NKIND_INDEX) {
        sprintf(printing, "(INDEX %d)", ast->numint);
        indent = 0;
    } else if (ast->mykind == NKIND_FUN_RSQ || ast->mykind == NKIND_FUN_DP3 || ast->mykind == NKIND_FUN_LIT) {
        char * name = ast->iden;
        if (ast->mykind == NKIND_FUN_RSQ) {
            name = "rsq";
        } else if (ast->mykind == NKIND_FUN_DP3) {
            name = "dp3";

        } else {
            name = "lit";

        }
        sprintf(printing, "call %s  ", name);

    }

    // if there is something to print
    if (printing[0] != 0) {

        // printing next line
        if (myline != lastprintline)
            fprintf(dumpFile, "\n");

        // printing scope
        int myscopecount = 0;

        // if this is a scope output, need to reduce one indent
        if (ast->mykind == NKIND_SCOPE)
            myscopecount = 1;


        // printing indentation (only if it needs)
        for (; indent && myscopecount < *scopelevel; myscopecount++)
            fprintf(dumpFile, "\t");

        // print the value
        fprintf(dumpFile, printing);

        // update the last printed line
        lastprintline = myline;

        // there are some type that do not require closing bracket
        if (ast->mykind == NKIND_INT ||
                ast->mykind == NKIND_FLOAT ||
                ast->mykind == NKIND_BOOL ||
                ast->mykind == NKIND_IDENT ||
                ast->mykind == NKIND_EXP_CAST ||
                ast->mykind == NKIND_DECLARATION)
            printing[0] = (char) 0;
    }

    /* Done checking this node! traverse to children */

    for (loopchild; traverse && loopchild < ast->nkids; loopchild++) {
        ast_print(ast->kids[loopchild], scopelevel, lastprintline);
    }

    /* Scope: close scope */
    if (ast->mykind == NKIND_PROGRAM || ast->mykind == NKIND_SCOPE) {
        popVariable();
        *scopelevel = *scopelevel - 1;
        fprintf(dumpFile, "\n");
    }

    // closing bracket
    if (printing[0] != 0) fprintf(dumpFile, ")");
}