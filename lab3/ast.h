#ifndef _AST_H
#define _AST_H

#include <stdarg.h>

// Dummy node just so everything compiles, create your own node/nodes
//
// The code provided below is an example ONLY. You can use/modify it,
// but do not assume that it is correct or complete.
//
// There are many ways of making AST nodes. The approach below is an example
// of a descriminated union. If you choose to use C++, then I suggest looking
// into inheritance.

// forward declare
struct node;
extern node *ast;

typedef enum {
    NKIND_FAIL,

    // non-ternminals node type
    NKIND_PROGRAM,
    NKIND_SCOPE,
    NKIND_INSTRUCTIONS,
    //NKINDS_VALUE,
    //NKINDS_TYPE,
    NKIND_INDEX,
    NKIND_DECLARATION,
    //NKIND_ASSIGNMENT,
    NKIND_ASSIGNMENT_REGULAR,
    NKIND_ASSIGNMENT_INDEX,
    NKIND_ASSIGNMENT_VEC2,
    NKIND_ASSIGNMENT_VEC3,
    NKIND_ASSIGNMENT_VEC4,
    //NKIND_EXPRESSION,
    NKIND_IF,
    NKIND_EIF,
    NKIND_ELSE,
    NKIND_STATEMENT,
    NKIND_LOOP,
    //NKIND_COMPARE,


    // qualifier
    NKIND_DECLARE_CONST,

    // functions
    NKIND_FUN_RSQ,
    NKIND_FUN_DP3,
    NKIND_FUN_LIT,

    // ternminal node values
    NKIND_INT, //20
    NKIND_FLOAT,
    NKIND_BOOL,
    NKIND_IDENT_INDEX,
    NKIND_IDENT,

    // terminal node type
    NKIND_TYPE_INT,
    NKIND_TYPE_BOOL,
    NKIND_TYPE_FLOAT,

    NKIND_TYPE_BVEC2,
    NKIND_TYPE_BVEC3,
    NKIND_TYPE_BVEC4,//30

    NKIND_TYPE_IVEC2,
    NKIND_TYPE_IVEC3,
    NKIND_TYPE_IVEC4,

    NKIND_TYPE_VEC2,
    NKIND_TYPE_VEC3,
    NKIND_TYPE_VEC4,


    // expression
    NKIND_EXP_PLUS,
    NKIND_EXP_MINUS,
    NKIND_EXP_MULTI,
    NKIND_EXP_DIVIDE,
    NKIND_EXP_NEG,
    NKIND_EXP_EXPO,

    NKIND_EXP_COMP,
    NKIND_EXP_AND,
    NKIND_EXP_OR,
    NKIND_EXP_NOT,

    NKIND_EXP_CAST,

    // compare
    NKIND_COMP_EQUAL,
    NKIND_COMP_NOT_EQUAL,
    NKIND_COMP_GREATER,
    NKIND_COMP_LESS,
    NKIND_COMP_GREATER_EQUAL,
    NKIND_COMP_LESS_EQUAL
} node_kind;

struct node {
    node_kind mykind;

    // node value
    union {
        int numint;
        float numfloat;
        char* iden;
    };

    int linenumber;
    int nkids;
    struct node * kids[1];

};

/* called in parser.y, each token identifed will dynamically create a node
 * also insert previous created node into kids list
 * on each token identifed, it already know how many kids is supposed to be created
 * 
 * For a node that should have a value (i.e. NKIND_INT, NKIND_FLOAT etc):
 * The first inputted arg is the value itself
 */
struct node *ast_allocate(node_kind mykind, int linenum, int kidcount, ...);

/* should be called when the tree is not useful anymore
 * It should be called with a postorder manner
 */
void ast_free(node *ast);


void ast_print(node * ast, int *scopelevel, int lastprintline);
#endif
