%{
/***********************************************************************
 * --YOUR GROUP INFO SHOULD GO HERE Weixin Wu 1000667405 ShunKong Cheung 1000774263--
 * 
 *   Interface to the parser module for CSC467 course project.
 * 
 *   Phase 2: Implement context free grammar for source language, and
 *            parse tracing functionality.
 *   Phase 3: Construct the AST for the source language program.
 ***********************************************************************/

/***********************************************************************
 *  C Definitions and external declarations for this module.
 *
 *  Phase 3: Include ast.h if needed, and declarations for other global or
 *           external vars, functions etc. as needed.
 ***********************************************************************/

#include <string.h>
#include "common.h"
#include "ast.h"
#include "symbol.h"
#include "semantic.h"

#include "parser.tab.h"


#define YYERROR_VERBOSE
#define yTRACE(x)    { if (traceParser) fprintf(traceFile, "%s\n", x); }

void yyerror(const char* s);    /* what to do in case of error              */
int yylex();                    /* procedure for calling lexical analyzer   */
extern int yyline;              /* variable holding current line number     */

%}

/***********************************************************************
 *  Yacc/Bison declarations.
 *  Phase 2:
 *    1. Add precedence declarations for operators (after %start declaration)
 *    2. If necessary, add %type declarations for some nonterminals
 *  Phase 3:
 *    1. Add fields to the union below to facilitate the construction of the
 *       AST (the two existing fields allow the lexical analyzer to pass back
 *       semantic info, so they shouldn't be touched).
 *    2. Add <type> modifiers to appropriate %token declarations (using the
 *       fields of the union) so that semantic information can by passed back
 *       by the scanner.
 *    3. Make the %type declarations for the language non-terminals, utilizing
 *       the fields of the union as well.
 ***********************************************************************/

%{
#define YYDEBUG 1 
%}


// TODO:Modify me to add more data types
// Can access me from flex using yylval
%union {
    int     numint;
    float   numfloat;
    char*   iden;
    struct node*   as_ast;
}

%token   

// value(s)
TOKEN_VAL_INTEGER
TOKEN_VAL_FLOAT
TOKEN_VAL_IDENTIFIER

// value: true/false
TOKEN_VAL_TRUE
TOKEN_VAL_FALSE

// type: value
TOKEN_TYP_INT
TOKEN_TYP_BOOL
TOKEN_TYP_FLOAT

// type: bool vector
TOKEN_TYP_BVEC2
TOKEN_TYP_BVEC3
TOKEN_TYP_BVEC4

// type: integer vector
TOKEN_TYP_IVEC2
TOKEN_TYP_IVEC3
TOKEN_TYP_IVEC4

// type: float vector
TOKEN_TYP_VEC2
TOKEN_TYP_VEC3
TOKEN_TYP_VEC4

// qualifier
TOKEN_QUA_CONST

// condition
TOKEN_CON_IF
TOKEN_CON_ELSE
TOKEN_CON_WHILE

// predefined functions
TOKEN_FUN_DP3
TOKEN_FUN_RSQ
TOKEN_FUN_LIT

// comparison: single 
TOKEN_ASSIGN
TOKEN_NOT
TOKEN_GREATER
TOKEN_LESS
TOKEN_EXPONENT

// comparison: double
TOKEN_EQUAL
TOKEN_NOT_EQUAL
TOKEN_GREATER_EQUAL
TOKEN_LESS_EQUAL

// math operators
TOKEN_PLUS
TOKEN_MINUS
TOKEN_MULTIPLY
TOKEN_DIVIDE

TOKEN_AND
TOKEN_OR

// grammatical
TOKEN_GRA_BRACKET_OPEN
TOKEN_GRA_BRACKET_CLOSE

TOKEN_GRA_CURLY_OPEN
TOKEN_GRA_CURLY_CLOSE

TOKEN_GRA_SQUARE_OPEN
TOKEN_GRA_SQUARE_CLOSE

TOKEN_GRA_COMMAR
TOKEN_GRA_COLON

//Lower the rule, the higher the precendence
//Left associatitivity vs. right associativity
%left TOKEN_OR
%left TOKEN_AND
%left TOKEN_EQUAL TOKEN_NOT_EQUAL TOKEN_GREATER_EQUAL TOKEN_LESS_EQUAL TOKEN_GREATER TOKEN_LESS
%left TOKEN_PLUS TOKEN_MINUS
%left TOKEN_MULTIPLY TOKEN_DIVIDE 
%right TOKEN_EXPONENT	
%left NEG
%precedence ASSIGN_REG

// node pointer
%type <as_ast> program
%type <as_ast> scope
%type <as_ast> instructions
%type <as_ast> instruction
%type <as_ast> declaration
%type <as_ast> declare_const
%type <as_ast> assignment
%type <as_ast> assignment_index
%type <as_ast> loop
%type <as_ast> value
%type <as_ast> identifier
%type <as_ast> statement
%type <as_ast> expression

%type <as_ast> statement_if
%type <as_ast> statement_elseif
%type <as_ast> comparison

%type <as_ast> index
%type <as_ast> type

// actual value
%type <numfloat> TOKEN_VAL_FLOAT
%type <numint> TOKEN_VAL_TRUE
%type <numint> TOKEN_VAL_FALSE
%type <numint> TOKEN_VAL_INTEGER
%type <iden> TOKEN_VAL_IDENTIFIER

%start    program

%%

/***********************************************************************
 *  Yacc/Bison rules
 *  Phase 2:
 *    1. Replace grammar found here with something reflecting the source
 *       language grammar
 *    2. Implement the trace parser option of the compiler
 *  Phase 3:
 *    1. Add code to rules for construction of AST.
 ***********************************************************************/
program
  : scope {ast = ast_allocate(NKIND_PROGRAM, yyline, 1, $1); yTRACE("program -> scope");}

scope
  :TOKEN_GRA_CURLY_OPEN instructions TOKEN_GRA_CURLY_CLOSE { $$ = ast_allocate(NKIND_SCOPE, yyline, 1, $2); yTRACE("scope -> {instructions}");}
;


instructions
  :instructions instruction		{yTRACE("instructions -> instructions instruction"); $$ = ast_allocate(NKIND_INSTRUCTIONS, yyline, 2, $1, $2); }
  | scope                               {$$ = $1;}
  |                                     {$$ = NULL;}
  ;

instruction
  :     declaration TOKEN_GRA_COLON 	{$$ = $1;yTRACE("instruction -> declaration\n");}
  |     declare_const                   {$$ = $1;yTRACE("instruction -> declare constant\n");}
  |     assignment  TOKEN_GRA_COLON   	{$$ = $1;yTRACE("instruction -> assignment\n");}
  |     assignment_index TOKEN_GRA_COLON {$$ = $1;yTRACE("instruction -> assignment_index\n");}
  |	statement    			{$$ = $1;yTRACE("instruction -> statement\n");}

  |	loop      			{$$ = $1;yTRACE("instruction -> loop\n");}
  | 	TOKEN_GRA_COLON                 {$$ = NULL;}
  ;

value
  :	TOKEN_VAL_FLOAT			{yTRACE("value -> float"); $$ = ast_allocate(NKIND_FLOAT,yyline,  0, $1);}
  |	TOKEN_VAL_TRUE			{yTRACE("value -> true"); $$ = ast_allocate(NKIND_BOOL,yyline,  0, 1);}
  |     TOKEN_VAL_FALSE			{yTRACE("value -> false");$$ = ast_allocate(NKIND_BOOL,yyline,  0, 0);}
  |     TOKEN_VAL_INTEGER		{yTRACE("value -> integer");$$ = ast_allocate(NKIND_INT, yyline, 0, $1);}
  |     identifier index                {yTRACE("value -> identifer[index]");$$ = ast_allocate(NKIND_IDENT_INDEX, yyline, 2, $1, $2);}
  |     identifier                      {yTRACE("value -> identifer[index]");$$ = $1;}
  |     TOKEN_FUN_RSQ TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_BRACKET_CLOSE
                                        {yTRACE("value -> rsq(exp)");$$ = ast_allocate(NKIND_FUN_RSQ, yyline, 1, $3);}
  |     TOKEN_FUN_DP3 TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_COMMAR expression TOKEN_GRA_BRACKET_CLOSE
                                        {yTRACE("value -> dp3(exp, exp)");$$ = ast_allocate(NKIND_FUN_DP3, yyline, 2, $3, $5);} 
   |     TOKEN_FUN_LIT TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_BRACKET_CLOSE
                                        {yTRACE("value -> lit(exp)");$$ = ast_allocate(NKIND_FUN_LIT, yyline, 1, $3);} 
  ;

type
  :	TOKEN_TYP_INT 			{yTRACE("type -> int");$$ = ast_allocate(NKIND_TYPE_INT, yyline, 0);}
  | 	TOKEN_TYP_BOOL  		{yTRACE("type -> bool");$$ = ast_allocate(NKIND_TYPE_BOOL, yyline, 0);}
  | 	TOKEN_TYP_FLOAT 		{yTRACE("type -> bvec2");$$ = ast_allocate(NKIND_TYPE_FLOAT, yyline, 0);}
  |     TOKEN_TYP_BVEC2  		{yTRACE("type -> bvec3");$$ = ast_allocate(NKIND_TYPE_BVEC2, yyline, 0);}
  | 	TOKEN_TYP_BVEC3  		{yTRACE("type -> bvec3");$$ = ast_allocate(NKIND_TYPE_BVEC3, yyline, 0);}
  | 	TOKEN_TYP_BVEC4 		{yTRACE("type -> bvec4");$$ = ast_allocate(NKIND_TYPE_BVEC4, yyline, 0);}
  |     TOKEN_TYP_IVEC2  		{yTRACE("type -> ivec2");$$ = ast_allocate(NKIND_TYPE_IVEC2, yyline, 0);}
  | 	TOKEN_TYP_IVEC3  		{yTRACE("type -> ivec3");$$ = ast_allocate(NKIND_TYPE_IVEC3, yyline, 0);}
  | 	TOKEN_TYP_IVEC4 		{yTRACE("type -> ivec4");$$ = ast_allocate(NKIND_TYPE_IVEC4, yyline, 0);}
  |     TOKEN_TYP_VEC2   		{yTRACE("type -> vec2");$$ = ast_allocate(NKIND_TYPE_VEC2, yyline, 0);}
  | 	TOKEN_TYP_VEC3   		{yTRACE("type -> vec3");$$ = ast_allocate(NKIND_TYPE_VEC3, yyline, 0);}
  | 	TOKEN_TYP_VEC4 			{yTRACE("type -> vec4");$$ = ast_allocate(NKIND_TYPE_VEC4, yyline, 0);}
  ;

// assign
declare_const:
        TOKEN_QUA_CONST type assignment     {$$ = ast_allocate(NKIND_DECLARE_CONST, yyline, 2, $2, $3);}
   ;

index
  //: TOKEN_GRA_SQUARE_OPEN TOKEN_VAL_INTEGER TOKEN_GRA_SQUARE_CLOSE index	{} // we suspect that MiniGL does not support array. Index is only useful vector
  : TOKEN_GRA_SQUARE_OPEN TOKEN_VAL_INTEGER TOKEN_GRA_SQUARE_CLOSE	{$$ = ast_allocate(NKIND_INDEX, yyline, 0,$2);}
  ;

identifier
  :     TOKEN_VAL_IDENTIFIER            {$$ = ast_allocate(NKIND_IDENT, yyline, 0,$1);}
  ;

declaration
  :	type identifier                 {yTRACE("declaration -> type identifier");$$ = ast_allocate(NKIND_DECLARATION, yyline, 2,$1,$2);}				
  |     type assignment			{yTRACE("declaration -> type assignment"); $$ = ast_allocate(NKIND_DECLARATION, yyline, 2,$1,$2);}	
  ;

assignment
  :identifier TOKEN_ASSIGN TOKEN_TYP_VEC2 TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_COMMAR expression TOKEN_GRA_BRACKET_CLOSE {$$ = ast_allocate(NKIND_ASSIGNMENT_VEC2, yyline, 3,$1,$5,$7); yTRACE("assignment -> identifier index = expression (vec2)");}  
| identifier TOKEN_ASSIGN TOKEN_TYP_IVEC2 TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_COMMAR expression TOKEN_GRA_BRACKET_CLOSE {$$ = ast_allocate(NKIND_ASSIGNMENT_VEC2, yyline, 3,$1,$5,$7); yTRACE("assignment -> identifier index = expression (vec2)");}  
| identifier TOKEN_ASSIGN TOKEN_TYP_BVEC2 TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_COMMAR expression TOKEN_GRA_BRACKET_CLOSE {$$ = ast_allocate(NKIND_ASSIGNMENT_VEC2, yyline, 3,$1,$5,$7); yTRACE("assignment -> identifier index = expression (vec2)");}  

| identifier TOKEN_ASSIGN TOKEN_TYP_VEC3 TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_COMMAR expression TOKEN_GRA_COMMAR expression TOKEN_GRA_BRACKET_CLOSE {$$ = ast_allocate(NKIND_ASSIGNMENT_VEC2, yyline, 3,$1,$5,$7,$9); yTRACE("assignment -> identifier index = expression (vec2)");}  
| identifier TOKEN_ASSIGN TOKEN_TYP_IVEC3 TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_COMMAR expression TOKEN_GRA_COMMAR expression TOKEN_GRA_BRACKET_CLOSE {$$ = ast_allocate(NKIND_ASSIGNMENT_VEC2, yyline, 3,$1,$5,$7,$9); yTRACE("assignment -> identifier index = expression (vec2)");}  
| identifier TOKEN_ASSIGN TOKEN_TYP_BVEC3 TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_COMMAR expression TOKEN_GRA_COMMAR expression TOKEN_GRA_BRACKET_CLOSE {$$ = ast_allocate(NKIND_ASSIGNMENT_VEC2, yyline, 3,$1,$5,$7,$9); yTRACE("assignment -> identifier index = expression (vec2)");}  
  | identifier TOKEN_ASSIGN TOKEN_TYP_VEC4 TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_COMMAR expression TOKEN_GRA_COMMAR expression TOKEN_GRA_COMMAR expression TOKEN_GRA_BRACKET_CLOSE {$$ = ast_allocate(NKIND_ASSIGNMENT_VEC4, yyline, 5,$1,$5,$7,$9,$11);yTRACE("assignment -> identifier index = expression (vec4)");}
  | identifier TOKEN_ASSIGN TOKEN_TYP_IVEC4 TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_COMMAR expression TOKEN_GRA_COMMAR expression TOKEN_GRA_COMMAR expression TOKEN_GRA_BRACKET_CLOSE {$$ = ast_allocate(NKIND_ASSIGNMENT_VEC4, yyline, 5,$1,$5,$7,$9,$11);yTRACE("assignment -> identifier index = expression (vec4)");}
  | identifier TOKEN_ASSIGN TOKEN_TYP_BVEC4 TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_COMMAR expression TOKEN_GRA_COMMAR expression TOKEN_GRA_COMMAR expression TOKEN_GRA_BRACKET_CLOSE {$$ = ast_allocate(NKIND_ASSIGNMENT_VEC4, yyline, 5,$1,$5,$7,$9,$11);yTRACE("assignment -> identifier index = expression (vec4)");}

  | identifier TOKEN_ASSIGN expression %prec ASSIGN_REG {yTRACE("assignment -> identifier = expression");$$ = ast_allocate(NKIND_ASSIGNMENT_REGULAR, yyline, 2,$1,$3);}	
  ;
assignment_index
  : identifier index TOKEN_ASSIGN expression {yTRACE("assignment_regular -> identifier index = expression");$$ = ast_allocate(NKIND_ASSIGNMENT_INDEX, yyline, 3,$1,$2, $4);}	
  ;

expression
  :	value 					{$$ = $1;yTRACE("expression -> value"); }
  |     expression TOKEN_PLUS expression	{$$ = ast_allocate(NKIND_EXP_PLUS, yyline, 2,$1,$3);yTRACE("expression -> exp + exp");}
  |     expression TOKEN_MINUS expression	{$$ = ast_allocate(NKIND_EXP_MINUS, yyline, 2,$1,$3);yTRACE("expression -> exp - exp");}
  |     expression TOKEN_MULTIPLY expression	{$$ = ast_allocate(NKIND_EXP_MULTI, yyline, 2,$1,$3);yTRACE("expression -> exp * exp");}
  |     expression TOKEN_DIVIDE expression	{$$ = ast_allocate(NKIND_EXP_DIVIDE, yyline, 2,$1,$3);yTRACE("expression -> exp / exp");}
  | 	TOKEN_MINUS expression %prec NEG	{$$ = ast_allocate(NKIND_EXP_NEG, yyline, 1, $2);yTRACE("expression -> -expression");}
  | 	expression TOKEN_EXPONENT expression	{$$ = ast_allocate(NKIND_EXP_EXPO, yyline, 2,$1,$3);yTRACE("expression -> expression ^ expression");}

  |	expression comparison expression	{$$ = ast_allocate(NKIND_EXP_COMP, yyline, 3,$1,$2, $3);yTRACE("expression -> expression compare expression");}
  | 	expression TOKEN_AND expression		{$$ = ast_allocate(NKIND_EXP_AND, yyline, 2,$1,$3);yTRACE("expression -> expression && expression");}
  |	expression TOKEN_OR expression		{$$ = ast_allocate(NKIND_EXP_OR, yyline, 2,$1,$3);yTRACE("expression -> expression || expression");}
  |	TOKEN_NOT expression			{$$ = ast_allocate(NKIND_EXP_NOT, yyline, 1,$2);yTRACE("expression -> !expression");}	      

  |	TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_BRACKET_CLOSE {$$ = $2; yTRACE("expression -> (expression)");}
  
  |   TOKEN_GRA_BRACKET_OPEN type TOKEN_GRA_BRACKET_CLOSE expression  
                                                {$$ = ast_allocate(NKIND_EXP_CAST, yyline, 2, $2, $4);yTRACE("expression -> !expression");}	      
  ;

statement_if: TOKEN_CON_IF TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_BRACKET_CLOSE scope
    {$$ = ast_allocate(NKIND_IF, yyline, 2,$3, $5);}

    ;

statement_elseif
  :	statement_if TOKEN_CON_ELSE statement_if {$$ = ast_allocate(NKIND_EIF, yyline, 2, $1, $3);}	
  |	statement_elseif TOKEN_CON_ELSE statement_if {$$ = ast_allocate(NKIND_EIF, yyline, 2, $1, $3);}
  ;

statement
  :	statement_if  {$$ = $1; yTRACE("statement -> if(exp){...}");}

  | 	statement_elseif {$$ = $1; yTRACE("statement -> if(exp){...} else if(cond){...}");}

  |	statement_elseif TOKEN_CON_ELSE scope
	    {$$ = ast_allocate(NKIND_ELSE, yyline, 2, $1, $3); yTRACE("statement -> if(exp){...} else if(exp){...} else {...}");}

  |	statement_if TOKEN_CON_ELSE scope
	   {$$ = ast_allocate(NKIND_ELSE, yyline, 2, $1, $3); yTRACE("statement -> if(exp){...}else(cond){...}");}
  ;

loop
  :	TOKEN_CON_WHILE TOKEN_GRA_BRACKET_OPEN expression TOKEN_GRA_BRACKET_CLOSE scope
		{$$ = ast_allocate(NKIND_LOOP, yyline, 2, $3, $5); 
                yTRACE("loop -> while(cond){instructions}");}
  ;
 
comparison
  :	TOKEN_EQUAL 		{$$ = ast_allocate(NKIND_COMP_EQUAL, yyline, 0); yTRACE("comp -> ==");}
  | 	TOKEN_NOT_EQUAL 	{$$ = ast_allocate(NKIND_COMP_NOT_EQUAL, yyline, 0); yTRACE("comp -> <=");}
  | 	TOKEN_GREATER_EQUAL 	{$$ = ast_allocate(NKIND_COMP_GREATER_EQUAL, yyline, 0); yTRACE("comp -> >=");}
  | 	TOKEN_LESS_EQUAL 	{$$ = ast_allocate(NKIND_COMP_LESS_EQUAL, yyline, 0); yTRACE("comp -> !=");}
  | 	TOKEN_GREATER 		{$$ = ast_allocate(NKIND_COMP_GREATER, yyline, 0); yTRACE("comp -> >");}
  | 	TOKEN_LESS		{$$ = ast_allocate(NKIND_COMP_LESS, yyline, 0); yTRACE("comp -> <");}
  ;
%%
/***********************************************************************ol
 * Extra C code.
 *
 * The given yyerror function should not be touched. You may add helper
 * functions as necessary in subsequent phases.
 ***********************************************************************/
void yyerror(const char* s) {
  if (errorOccurred)
    return;    /* Error has already been reported by scanner */
  else
    errorOccurred = 1;
        
  fprintf(errorFile, "\nPARSER ERROR, LINE %d",yyline);
  if (strcmp(s, "parse error")) {
    if (strncmp(s, "parse error, ", 13))
      fprintf(errorFile, ": %s\n", s);
    else
      fprintf(errorFile, ": %s\n", s+13);
  } else
    fprintf(errorFile, ": Reading token %s\n", yytname[YYTRANSLATE(yychar)]);
}

